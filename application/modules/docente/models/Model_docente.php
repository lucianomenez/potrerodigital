<?php

/**
 * Funciones para el manejo de datos del container inscripciones
 * @author Luciano Menez <lucianomenez1212@gmail.com>
 * @date 3/05/2016
 *
 */

class Model_docente extends CI_Model {

    public function __construct() {
        // Call the Model constructor
        parent::__construct();
        $this->load->library('cimongo/cimongo', '', 'db');
    }

    function get_cursos($id = null){
        $result = array();
        $container = 'potrerodigital.cursos';
        if ($id != null){
          $query=array();
          $this->db->where($query);
          $result = $this->db->get($container)->result_array();
          return $result[0];
        }else{
          $result = $this->db->get($container)->result_array();
          return $result;
        }
    }

    function get_clases_por_curso($id = null){
        $result = array();
        $container = 'potrerodigital.clases';
        $query= array('id_curso' => $id, 'publicada' => true);
        $this->db->where($query);
        $result = $this->db->get($container)->result_array();
        return $result;

    }

    function get_clase($id = null){
        $result = array();
        $container = 'potrerodigital.clases';
        $query= array('id_clase' => intval($id));
        $this->db->where($query);
        $result = $this->db->get($container)->result_array();
        return $result;
    }

    function get_tareas($id = null){
        $result = array();
        $container = 'potrerodigital.tareas';
        if ($id != null){
          $query=array();
          $this->db->where($query);
          $result = $this->db->get($container)->result_array();
          return $result[0];
        }else{
          $result = $this->db->get($container)->result_array();
          return $result;
        }
    }

    function get_carreras($id = null){
        $result = array();
        $container = 'potrerodigital.carreras';
        if ($id != null){
          $query=array();
          $this->db->where($query);
          $result = $this->db->get($container)->result_array();
          return $result[0];
        }else{
          $result = $this->db->get($container)->result_array();
          return $result;
        }
    }

    function update_cursos($data){
        $result = array();
        $container = 'potrerodigital.cursos';
        $query= array('id' => $data['id']);
        $this->db->where($query);
        $result = $this->db->update($container, $data);
        return $result;
    }

    function update_clase($data){
        $result = array();
        $container = 'potrerodigital.clases';
        $query= array('id_clase' => intval($data['id_clase']));
        $this->db->where($query);
        $result = $this->db->update($container, $data);
        return $result;
    }

    function insert_clase($data){
      $container = 'potrerodigital.clases';
      $id = $this->get_last_id();
      $data["id_clase"] = ++$id;
      unset($data['id']);
      $this->db->insert($container, $data);
      return;
    }

    function get_last_id(){
      $container = 'potrerodigital.clases';
      $result = $this->db->get($container)->result_array();
      if (empty($result)){
        $result["id_clase"] = 0;
      }else{
        $result = end($result);
      }
      return $result['id_clase'];
    }

    function borrar_clase($data){
      $container = 'potrerodigital.clases';
      $query= array('id_clase' => intval($data['id']));
      $this->db->where($query);
      $this->db->delete($container);
      return;
    }


}
