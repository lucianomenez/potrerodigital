<?php

class Docente extends MX_Controller {

  function __construct() {
      parent::__construct();
      $this->base_url = base_url();
      $this->module_url = base_url() . $this->router->fetch_module() . '/';
      $this->user->authorize();
      $this->load->module('elements');
      $this->load->helper('file');
      $this->load->helper('url');
      $this->load->library('parser');
      $this->load->model('app');
      $this->load->model('alumno/Model_alumno');
      $this->load->model('Model_docente');
      $this->load->model('user/user');
      $this->idu = $this->user->idu;
      $this->alumno = $this->user->get_user($this->idu);

  }

    //==== MAIN ALUMNO

    function Index() {
      $data["base_url"] = $this->base_url;
      $data["module_url"] = $this->module_url;
      $data["idu"] = $this->idu;
      $data["head"] = $this->elements->head();
      $data["header"] = $this->elements->ui_header();
      $data["navbar"] = $this->elements->ui_navbar_docente();
      $data["cursos"] = $this->Model_alumno->get_cursos();
      foreach ($data['cursos'] as &$curso){
        $curso['clases'] = $this->Model_docente->get_clases_por_curso($curso["id"]);
      };
      echo $this->parser->parse("index", $data, true, true);
     	//$customData['usercan_create']=true;
    }

    function cursos() {
      $data["base_url"] = $this->base_url;
      $data["module_url"] = $this->module_url;
      $data["idu"] = $this->idu;
      $data["header"]= $this->elements->ui_header();
      $data["navbar"]= $this->elements->ui_navbar();
      $data["head"]= $data->elements->ui_head();
      echo $this->parser->parse("cursos", $data, true, true);
     	//$customData['usercan_create']=true;
    }

    function carreras() {
      $data["base_url"] = $this->base_url;
      $data["module_url"] = $this->module_url;
      $data["idu"] = $this->idu;
      $data["header"]= $this->elements->ui_header();
      $data["navbar"]= $this->elements->ui_navbar();
      $data["head"]= $data->elements->ui_head();
      echo $this->parser->parse("carreras", $data, true, true);
     	//$customData['usercan_create']=true;
    }


    function tareas() {
      $data["base_url"] = $this->base_url;
      $data["module_url"] = $this->module_url;
      $data["idu"] = $this->idu;
      $data["header"]= $this->elements->ui_header();
      $data["navbar"]= $this->elements->ui_navbar();
      $data["head"]= $data->elements->ui_head();
      echo $this->parser->parse("tareas", $data, true, true);
     	//$customData['usercan_create']=true;
    }






} //

?>
