<?php

class Api extends MX_Controller {

  function __construct() {
      parent::__construct();
      $this->base_url = base_url();
      $this->module_url = base_url() . $this->router->fetch_module() . '/';
      $this->user->authorize();
      $this->load->module('elements');
      $this->load->helper('file');
      $this->load->helper('url');
      $this->load->library('parser');
      $this->load->model('app');
      $this->load->model('user/user');
      $this->load->model('Model_docente');
      $this->load->model('alumno/Model_alumno');

      $this->idu = $this->user->idu;
      $this->alumno = $this->user->get_user($this->idu);

  }

    //==== MAIN DOCENTE

    function cargar_clase() {
      $data = $this->input->post();
      $data["fecha"] = date("d-m-Y");
      $data["nombre"] = $this->alumno->name." ".$this->alumno->lastname;
      $curso = $this->Model_alumno->get_cursos($data["id"]);
      $data['id_curso'] = $data["id"];
      $data['id_clase'] =$this->Model_docente->get_last_id();
      $data['id_clase'] = $data['id_clase'] + 1;
      if (!empty($_FILES)){
        $data['archivo_adjunto'] = $this->upload_file($_FILES, $data);
      }
      $this->Model_docente->insert_clase($data);
      redirect (base_url()."docente");
     	//$customData['usercan_create']=true;
    }

    function update_clase() {
      $data = $this->input->post();
      $data["fecha"] = date("d-m-Y");
      $data['id_clase'] = intval($data['id_clase']);
      $data["nombre"] = $this->alumno->name." ".$this->alumno->lastname;
      if (!empty($_FILES)){
        $data['archivo_adjunto'] = $this->upload_file($_FILES, $data);
      }
      $this->Model_docente->update_clase($data);
      redirect (base_url()."docente");
     	//$customData['usercan_create']=true;
    }


    function borrar_clase() {
      $data = $this->input->post();
      $this->Model_docente->borrar_clase($data);
      redirect (base_url()."docente");
      
     	//$customData['usercan_create']=true;
    }

    function get_clase() {
      $data = $this->input->post();
      $clase = $this->Model_docente->get_clase($data['id']);
      echo json_encode($clase[0]);
     	//$customData['usercan_create']=true;
    }

    function upload_file($data_file, $data_post){

//     $_FILES = $_FILES["archivo_adjunto"];
      $file_extension = pathinfo($data_file['archivo_adjunto']['name'], PATHINFO_EXTENSION);

  //    var_dump (FCPATH.'application/modules/archivos/assets/clases/'.$data_post['id_curso'].'/'.strval($data_post['id_clase']).'/');
    //  exit;
      // Make dir
      if (!file_exists(FCPATH.'application/modules/archivos/assets/clases/'.$data_post['id_curso'].'/'.strval($data_post['id_clase']).'/')){
           @mkdir(FCPATH.'application/modules/archivos/assets/clases/'.$data_post['id_curso'].'/'.strval($data_post['id_clase']).'/',0777,true);
      }

      // File configuration
      $config['file_name']            = "Clase-" .$data_post['id_clase'] . "-"  . date("Y-m-d")."." .$file_extension;
      $config['upload_path']          = FCPATH.'application/modules/archivos/assets/clases/'.$data_post['id_curso'].'/'.strval($data_post['id_clase']).'/';
      $config['allowed_types']        = "gif|jpg|png|doc|DOC|docx|DOCX|jpeg|txt|TXT|JPG|PNG|JPEG|pdf|PDF|mp3|MP3|mp4|MP4|";
      $config['max_size']            = 10000;
      $config['overwrite']           = false;

      // Upload
      $this->load->library('upload', $config);

      // Set response data
      $response['uploadUrl'] = $this->base_url.'archivos/assets/clases/'.$data_post['id_curso'].'/'.strval($data_post['id_clase']).'/'.$config['file_name'];
      if ( ! $this->upload->do_upload('archivo_adjunto'))
      {
          $error = array('error' => $this->upload->display_errors());
          var_dump($error, $config['upload_path']);
      }
      else
      {
          $data = array('upload_data' => $this->upload->data());
          $data['upload_path'] = $config['upload_path'];
          echo json_encode($response);
      }
      return $response['uploadUrl'];
    }



} //

?>
