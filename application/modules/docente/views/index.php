<!DOCTYPE html>
<html lang="en">
{head}
<link rel="stylesheet" href="{base_url}elements/assets/css/to-do.css">

<body>
  <section id="container">
    <input type="hidden" id="base_url" name="base_url" value="{base_url}">

    <!-- **********************************************************************************************************************************************************
        TOP BAR CONTENT & NOTIFICATIONS
        *********************************************************************************************************************************************************** -->
    <!--header start-->
    {header}
    <!--header end-->
    <!-- **********************************************************************************************************************************************************
        MAIN SIDEBAR MENU
        *********************************************************************************************************************************************************** -->
    <!--sidebar start-->
    {navbar}
    <!--sidebar end-->
    <!-- **********************************************************************************************************************************************************
        MAIN CONTENT
        *********************************************************************************************************************************************************** -->
    <!--main content start-->
    <section id="main-content">
      <section class="wrapper">
        <h3><i class="fa fa-angle-right"></i> Cursos Disponibles - Vista de Docente</h3>
        <!-- COMPLEX TO DO LIST -->
        {cursos}
        <div class="row mt">
          <div class="col-md-12">
            <section class="task-panel tasks-widget">
              <div class="panel-heading">
                <div class="pull-left">
                  <h5><i class="fa fa-tasks"></i>{nombre_curso}</h5>
                </div>
                <br>
              </div>
              <div class="panel-body">
                <div class="task-content">
                  <ul class="task-list">
                    {clases}
                    <li>
                      <div class="task-checkbox">
                      </div>
                      <div class="task-title">
                        <span class="task-title-sp">{nombre_clase}</span>
                        <span class="badge bg-theme">Nueva</span>
                        <div class="pull-right hidden-phone">
                          <a href="{archivo_adjunto}" target="_blank"><button class="btn btn-success btn-xs"><i class=" fa fa-download"></i></button></a>
                          <button class="btn btn-primary btn-xs"><i class="fa fa-eye"></i></button>
                          <a href="{link_externo}"><button class="btn btn-info btn-xs"><i class="fa fa-link"></i></button></a>
                          <button data-idclase="{id_clase}" class="btn btn-warning btn-xs modal_edit"><i class="fa fa-edit"></i></button>
                          <button data-idclase="{id_clase}" class="btn btn-danger btn-xs delete_clase"><i class="fa fa-remove"></i></button>

                        </div>
                      </div>
                    </li>
                    {/clases}
                  </ul>
                </div>
                <div class=" add-task-row">
                  <button class="btn btn-success btn-sm pull-left modal_trigger" data-idcurso="{id}" data-toggle="modal" data-target="#myModal">Agregar Clase</button>
                </div>
              </div>
            </section>
          </div>
          <!-- /col-md-12-->
        </div>
        {/cursos}
        <!-- /row -->
      </section>
      <!-- /wrapper -->
    </section>
    <!--main content end-->
    <!--footer start-->
    <!--footer end-->
  </section>

  <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <h4 class="modal-title" id="myModalLabel">Agregar Clase</h4>
        </div>
        <div class="modal-body">
          <form role="form" class="form-horizontal" method="post" action="{base_url}docente/api/cargar_clase" enctype="multipart/form-data">
            <input type="hidden" id="idu" name="creado_por" value="{idu}">
            <input type="hidden" id="id_curso" name="id">

            <div class="form-group">
              <label class="col-lg-2 control-label">Archivo adjunto</label>
              <div class="col-lg-6">
                <input type="file" name="archivo_adjunto" id="exampleInputFile" class="file-pos">
              </div>
            </div>
            <div class="form-group">
              <label class="col-lg-2 control-label">Título</label>
              <div class="col-lg-6">
                <input type="text" name="nombre_clase" placeholder="Título de la clase"  class="form-control" >
              </div>
            </div>
            <div class="form-group">
              <label class="col-lg-2 control-label">Sintesis</label>
              <div class="col-lg-6">
                <input type="text" name="sintesis" value="{name}" class="form-control">
              </div>
            </div>
            <div class="form-group">
              <label class="col-lg-2 control-label">Desarrollo de la clase</label>
              <div class="col-lg-6">
                <textarea rows="10" name="desarrollo" class="form-control"></textarea>
              </div>
            </div>
            <div class="form-group">
              <label class="col-lg-2 control-label">Link Externo</label>
              <div class="col-lg-6">
                <input type="text" name="link_externo" placeholder="http://www.ejemplo.com..." class="form-control">
              </div>
            </div>
            <div class="form-group">
              <label class="col-lg-2 control-label">Objetivos</label>
              <div class="col-lg-6">
                <textarea name="objetivos" rows="5" class="form-control"></textarea>
              </div>
            </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-danger" data-dismiss="modal">Salir</button>
          <button type="submit" class="btn btn-success">Enviar</button>
        </div>
      </form>
      </div>
    </div>
  </div>


  <div class="modal fade" id="Modal_edit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <h4 class="modal-title" id="myModalLabel">Editar Clase</h4>
        </div>
        <div class="modal-body">
          <form role="form" class="form-horizontal" enctype="multipart/form-data" method="post" action="{base_url}docente/api/update_clase">
            <input type="hidden" id="idu" name="creado_por" value="{idu}">
            <input type="hidden" id="id_clase" name="id_clase" value="">
            <input type="hidden" id="id_curso_edit" name="id_curso" value="">


            <div class="form-group">
              <label class="col-lg-2 control-label">Archivo adjunto</label>
              <div class="col-lg-6">
                <input type="file" name="archivo_adjunto" class="file-pos">
              </div>
            </div>
            <div class="form-group">
              <label class="col-lg-2 control-label">Título</label>
              <div class="col-lg-6">
                <input type="text" name="nombre_clase" id="nombre_clase" placeholder="Título de la clase"  class="form-control" >
              </div>
            </div>
            <div class="form-group">
              <label class="col-lg-2 control-label">Sintesis</label>
              <div class="col-lg-6">
                <input type="text" name="sintesis" id="sintesis" value="{name}" class="form-control">
              </div>
            </div>
            <div class="form-group">
              <label class="col-lg-2 control-label">Desarrollo de la clase</label>
              <div class="col-lg-6">
                <textarea rows="10" name="desarrollo" id="desarrollo" class="form-control"></textarea>
              </div>
            </div>
            <div class="form-group">
              <label class="col-lg-2 control-label">Link Externo</label>
              <div class="col-lg-6">
                <input type="text" name="link_externo" id="link_externo" placeholder="www.ejemplo.com..." class="form-control">
              </div>
            </div>
            <div class="form-group">
              <label class="col-lg-2 control-label">Objetivos</label>
              <div class="col-lg-6">
                <textarea name="objetivos" rows="5" id="objetivos" class="form-control"></textarea>
              </div>
            </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-danger" data-dismiss="modal">Salir</button>
          <button type="submit" class="btn btn-success">Guardar</button>
        </div>
      </form>
      </div>
    </div>
  </div>

  <!-- js placed at the end of the document so the pages load faster -->
  <script src="{base_url}elements/assets/lib/jquery/jquery.min.js"></script>
  <script src="{base_url}elements/assets/lib/bootstrap/js/bootstrap.min.js"></script>
  <script class="include" type="text/javascript" src="{base_url}elements/assets/lib/jquery.dcjqaccordion.2.7.js"></script>
  <script src="{base_url}elements/assets/lib/jquery.scrollTo.min.js"></script>
  <script src="{base_url}elements/assets/lib/jquery.nicescroll.js" type="text/javascript"></script>
  <script src="{base_url}elements/assets/lib/jquery.sparkline.js"></script>
  <!--common script for all pages-->
  <script src="{base_url}elements/assets/lib/common-scripts.js"></script>
  <script type="text/javascript" src="{base_url}elements/assets/lib/gritter/js/jquery.gritter.js"></script>
  <script type="text/javascript" src="{base_url}elements/assets/lib/gritter-conf.js"></script>
  <!--script for this page-->
  <script src="{base_url}elements/assets/lib/sparkline-chart.js"></script>
  <script src="{base_url}elements/assets/lib/zabuto_calendar.js"></script>
  <script src="{base_url}alumno/assets/js/app.js"></script>

</body>

</html>
