<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="Dashboard">
  <meta name="keyword" content="Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">
  <title>Potrero Digital - Campus Virtual</title>

  <!-- Favicons -->
  <link href="{base_url}elements/assets/img/favicon.png" rel="icon">
  <link href="{base_url}elements/assets/img/apple-touch-icon.png" rel="apple-touch-icon">

  <!-- Bootstrap core CSS -->
  <link href="{base_url}elements/assets/lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <!--external css-->
  <link href="{base_url}elements/assets/lib/font-awesome/css/font-awesome.css" rel="stylesheet" />
  <!-- Custom styles for this template -->
  <link href="{base_url}elements/assets/css/style.css" rel="stylesheet">
  <link href="{base_url}elements/assets/css/style-responsive.css" rel="stylesheet">

</head>

<body>
  <!-- **********************************************************************************************************************************************************
      MAIN CONTENT
      *********************************************************************************************************************************************************** -->
  <div id="login-page">
    <div class="container">
      <form class="form-login" id="formAuth" action="{authUrl}" method="post">
        <h2 class="form-login-heading">Potrero Digital - Campus Virtual</h2>
        <div class="login-wrap">
          <input type="text" id="usernameInput" name="username" class="form-control" placeholder="Usuario" autofocus>
          <br>
          <input type="password" id="passwordInput" name="password" class="form-control" placeholder="Contraseña">
          <label class="checkbox">
            <span class="pull-right">
            <a data-toggle="modal" href="login.html#myModal"> Te olvidaste la contraseña?</a>
            </span>
            </label>
          <button class="btn btn-theme btn-block" href="index.html" type="submit"><i class="fa fa-lock"></i>Entrá</button>
        </form>
          <hr>
          <div class="registration">
            Solicitar inscripción<br/>

         <!-- Modal solicitar inscripcion-->
            <a href="" data-toggle="modal" data-target=".bannerformmodal">acá</a>

            <div class="modal fade bannerformmodal" tabindex="-1" role="dialog" aria-labelledby="bannerformmodal" aria-hidden="true">
              <div class="modal-dialog modal-sm">
                <div class="modal-content">
                  <div class="modal-content">
                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                      <h4 class="modal-title" id="myModalLabel">Solicitar inscripcion</h4>
                    </div>
                    <div class="modal-body">
                      <form id="requestacallform" method="POST" action="{base_url}perfil/solicitar_registro/" name="requestacallform">
                        <div class="form-group">
                          <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-user"></i></span>
                            <input id="Nombre" type="text" class="form-control" placeholder="Nombre" name="name"/>
                          </div>
                        </div>
                        <div class="form-group">
                          <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-user"></i></span>
                            <input id="Apellido" type="text" class="form-control" placeholder="Apellido" name="lastname"/>
                          </div>
                        </div>
                        <div class="form-group">
                          <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                            <input id="email1" type="text" class="form-control" placeholder="Email" name="email" onchange="validateEmailAdd();"/>
                          </div>
                        </div>
                        <div class="form-group">
                          <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-user"></i></span>
                            <input id="DNI" type="text" class="form-control" placeholder="DNI" name="{nick}"/>
                          </div>
                        </div>
                        <div class="form-group">
                          <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-user"></i></span>
                            <select class="custom-select" id="inputGroupSelect01" name="potrero">
                              <option disabled selected>Potrero de pertenencia</option>
                              <option value="Morón">Morón</option>
                              <option value="Morón">La Juanita</option>
                            </select>
                          </div>
                        </div>
                      </form>
                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-info">enviar</button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- Modal -->
        <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="myModal" class="modal fade">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Recuperar contraseña</h4>
              </div>
              <div class="modal-body">
                <p>Ingresa tu E-mail para recuperar la contraseña</p>
                <input type="text" name="email" placeholder="Email" autocomplete="off" class="form-control placeholder-no-fix">
              </div>
              <div class="modal-footer">
                <button data-dismiss="modal" class="btn btn-default" type="button">Cancelar</button>
                <button class="btn btn-theme" type="button">Enviar</button>
              </div>
            </div>
          </div>
        </div>
        <!-- modal -->
      </form>
    </div>
  </div>
  <!-- js placed at the end of the document so the pages load faster -->
  <script src="{base_url}elements/assets/lib/jquery/jquery.min.js"></script>
  <script src="{base_url}elements/assets/lib/bootstrap/js/bootstrap.min.js"></script>
  <script src="{base_url}alumno/assets/js/app.js"></script>

  <!--BACKSTRETCH-->
  <!-- You can use an image of whatever size. This script will stretch to fit in any screen size.-->
  <script type="text/javascript" src="{base_url}elements/assets/lib/jquery.backstretch.min.js"></script>
  <script>
    $.backstretch("{base_url}elements/assets/img/login-bg.jpg", {
      speed: 500
    });

  </script>
</body>

</html>
