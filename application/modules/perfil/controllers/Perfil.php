<?php

class Perfil extends MX_Controller {

  function __construct() {
      parent::__construct();
      $this->base_url = base_url();
      $this->module_url = base_url() . $this->router->fetch_module() . '/';
  //    $this->user->authorize()
      $this->load->module('elements');
      $this->load->helper('file');
      $this->load->helper('url');
      $this->load->library('parser');
      $this->load->model('app');
      $this->load->model('user/user');
      $this->load->model('Model_perfil');

      $this->idu = $this->user->idu;
      $this->alumno = $this->user->get_user($this->idu);

  }

    //==== MAIN ALUMNO

    function Index() {
      $data["base_url"] = $this->base_url;
      $data["module_url"] = $this->module_url;
      $data["idu"] = $this->idu;
      $data["head"] = $this->elements->head();
      $data["header"] = $this->elements->ui_header();
      $data["navbar"] = $this->elements->ui_navbar();
      $data['usuario'][0] = $this->user->get_user_array($this->idu);
      echo $this->parser->parse("index", $data, true, true);
     	//$customData['usercan_create']=true;
    }

    function solicitud_pendiente(){
      $data["base_url"] = $this->base_url;
      $data["module_url"] = $this->module_url;
      $data["nombre"] = $nombre;
      $data["apellido"] = $apellido;
      echo $this->parser->parse("solicitud_pendiente", $data, true, true);
    }

    function institucional(){
      $data["base_url"] = $this->base_url;
      $data["module_url"] = $this->module_url;
      $data["idu"] = $this->idu;
      $data["head"] = $this->elements->head();
      $data["header"] = $this->elements->ui_header();
      $data["navbar"] = $this->elements->ui_navbar();
      echo $this->parser->parse("institucional", $data, true, true);
    }

    function recuperar_contrasena(){
      $data = $this->input->post();
      $data["base_url"] = $this->base_url;
      $data["module_url"] = $this->module_url;
      echo $this->parser->parse("recuperar_contrasena", $data, true, true);
    }

    function ver_perfil($idu) {
      $data["base_url"] = $this->base_url;
      $data["module_url"] = $this->module_url;
      $data["idu"] = $this->idu;
      $data["header"]= $this->elements->ui_header();
      $data["navbar"]= $this->elements->ui_navbar();
      $data["head"]= $this->elements->head();

     	//$customData['usercan_create']=true;
    }

    function register() {
      $data = $this->input->post();
      $this->user->insert_user();
     	//$customData['usercan_create']=true;
    }

    function solicitar_inscripcion(){
      $data = $this->input->post();
      $data["estado"] = "pendiente";
      $this->Model_perfil->insertar_usuario_temporal($data);
      redirect(base_url() . 'perfil/solicitud_pendiente/'.$data['name'].'/'.$data['lastname']);

    }


    function update_profile() {
      $data = $this->input->post();
      $data['idu'] = $this->idu;
      if (!empty($_FILES)){
        $data['avatar_img'] = $this->upload_file($_FILES, $data);
      }
      $this->user->update($data);
  //    redirect(base_url() . "perfil");
     	//$customData['usercan_create']=true;
    }

    function upload_file($data_file, $data_post){

//     $_FILES = $_FILES["archivo_adjunto"];
      $file_extension = pathinfo($data_file['avatar_img']['name'], PATHINFO_EXTENSION);


      // Make dir
      @mkdir(FCPATH.'application/modules/perfil/assets/images/'.$data_post['idu'].'/',0777,true);


      // File configuration
      $config['file_name']            = "profile.jpg";
      $config['upload_path']          = FCPATH.'application/modules/perfil/assets/images/'.$data_post['idu'].'/';
      $config['allowed_types']        = "gif|jpg|png|jpeg|JPG|PNG|JPEG|";
      $config['max_size']            = 10000;
      $config['overwrite']           = true;

      // Upload
      $this->load->library('upload', $config);

      // Set response data
      $response['uploadUrl'] = $this->base_url.'archivos/perfil/assets/images/'.$data_post['idu'].'/'.$config['file_name'];

      if ( ! $this->upload->do_upload('avatar_img'))
      {
          $error = array('error' => $this->upload->display_errors());
          var_dump($error, $config['upload_path']);
      }
      else
      {
          $data = array('upload_data' => $this->upload->data());
          $data['upload_path'] = $config['upload_path'];
          echo json_encode($response);
      }
      redirect ($this->module_url);
    }






} //

?>
