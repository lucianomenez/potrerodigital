<!DOCTYPE html>
<html lang="en">
{head}
<body>
  <section id="container">
    <!-- **********************************************************************************************************************************************************
        TOP BAR CONTENT & NOTIFICATIONS
        *********************************************************************************************************************************************************** -->
    <!--header start-->
    {header}
    <!--header end-->
    <!-- **********************************************************************************************************************************************************
        MAIN SIDEBAR MENU
        *********************************************************************************************************************************************************** -->
    <!--sidebar start-->
    {navbar}
    <!--sidebar end-->
    <!-- **********************************************************************************************************************************************************
        MAIN CONTENT
        *********************************************************************************************************************************************************** -->
    <!--main content start-->
    <section id="main-content">
      <section class="wrapper site-min-height">
        <div class="row mt">
          {usuario}
          <div class="col-lg-12">
            <div class="row content-panel">
              <div class="col-md-4 profile-text mt mb centered">
                <div class="right-divider hidden-sm hidden-xs">
                  <h4>0</h4>
                  <h6>Cursos Completados</h6>
                  <h4>0</h4>
                  <h6>Clases Completadas</h6>
                  <h4>0</h4>
                  <h6>Tareas Completadas</h6>
                </div>
              </div>
              <!-- /col-md-4 -->
              <div class="col-md-4 profile-text">
                <h1>{name} {lastname}</h1>
                <h3>{rol}</h3>
                <p>{descripcion_personal}</p>
                <br>
              </div>
              <!-- /col-md-4 -->
              <div class="col-md-4 centered">
                <div class="profile-pic">
                  <p><img src="{base_url}perfil/assets/images/{idu}/profile.jpg"  onerror="this.onerror=null; this.src='{base_url}perfil/assets/images/avatar.jpg'" class="img-circle"></p>
                  <p>
                    <!-- <button class="btn btn-theme"><i class="fa fa-check"></i> Follow</button>
                    <button class="btn btn-theme02">Add</button> -->
                  </p>
                </div>
              </div>
              <!-- /col-md-4 -->
            </div>
            <!-- /row -->
          </div>
          <!-- /col-lg-12 -->
          <div class="col-lg-12 mt">
            <div class="row content-panel">
              <!-- /panel-heading -->
              <div class="panel-body">
                <div class="tab-content">
                  <!-- /tab-pane -->
                    <div class="row">
                      <div class="col-lg-8 col-lg-offset-2 detailed">
                        <h4 class="mb">Información Personal</h4>
                        <form role="form" class="form-horizontal" method="post" action="{base_url}perfil/update_profile"  enctype="multipart/form-data">
                          <div class="form-group">
                            <label class="col-lg-2 control-label">Avatar</label>
                            <div class="col-lg-6">
                              <input type="file" name="avatar_img" id="exampleInputFile" class="file-pos">
                            </div>
                          </div>
                          <div class="form-group">
                            <label class="col-lg-2 control-label">Usuario</label>
                            <div class="col-lg-6">
                              <input type="text" placeholder="" value="{nick}" class="form-control" readonly>
                            </div>
                          </div>
                          <div class="form-group">
                            <label class="col-lg-2 control-label">Nombre</label>
                            <div class="col-lg-6">
                              <input type="text" name="name" value="{name}" class="form-control">
                            </div>
                          </div>
                          <div class="form-group">
                            <label class="col-lg-2 control-label">Apellido</label>
                            <div class="col-lg-6">
                              <input type="text" name="lastname" value="{lastname}" class="form-control">
                            </div>
                          </div>
                          <div class="form-group">
                            <label class="col-lg-2 control-label">Provincia</label>
                            <div class="col-lg-6">
                              <input type="text" name="provincia" value="{provincia}"  class="form-control">
                            </div>
                          </div>
                          <div class="form-group">
                            <label class="col-lg-2 control-label">Localidad</label>
                            <div class="col-lg-6">
                              <input type="text" name="localidad" value="{localidad}" class="form-control">
                            </div>
                          </div>
                          <div class="form-group">
                            <label class="col-lg-2 control-label">Descripción Personal</label>
                            <div class="col-md-10">
                              <textarea rows="5" col="10" class="form-control" name="descripcion_personal">{descripcion_personal}</textarea>
                            </div>
                          </div>
                      </div>
                      <div class="col-lg-8 col-lg-offset-2 detailed mt">
                        <h4 class="mb">Información de Contacto</h4>
                        <div class="form-horizontal">
                          <div class="form-group">
                            <label class="col-lg-2 control-label">Domicilio</label>
                            <div class="col-lg-6">
                              <input type="text" name="address" value="{address}" placeholder=" " id="addr1" class="form-control">
                            </div>
                          </div>
                          <div class="form-group">
                            <label class="col-lg-2 control-label">Teléfono</label>
                            <div class="col-lg-6">
                              <input type="text" name="phone" value="{phone}" placeholder=" " id="phone" class="form-control">
                            </div>
                          </div>

                          <div class="form-group">
                            <label class="col-lg-2 control-label">Email</label>
                            <div class="col-lg-6">
                              <input type="text" name="email" value="{email}" placeholder=" " id="email" class="form-control">
                            </div>
                          </div>
                          <div class="form-group">
                            <label class="col-lg-2 control-label">Sitio Personal</label>
                            <div class="col-lg-6">
                              <input type="text" name="site" value="{site}" placeholder=" " id="site" class="form-control">
                            </div>
                          </div>
                          <div class="form-group">
                            <div class="col-lg-offset-2 col-lg-10">
                              <button class="btn btn-theme" type="submit">Enviar</button>
                            </div>
                          </div>
                        </form>
                      </div>
                      <!-- /col-lg-8 -->
                    </div>
                    <!-- /row -->
                  <!-- /tab-pane -->
                </div>
                <!-- /tab-content -->
              </div>
              <!-- /panel-body -->
            </div>
            <!-- /col-lg-12 -->
          </div>
          <!-- /row -->
        </div>
        {/usuario}
        <!-- /container -->
      </section>
      <!-- /wrapper -->
    </section>
    <!-- js placed at the end of the document so the pages load faster -->
    <script src="{base_url}elements/assets/lib/jquery/jquery.min.js"></script>
    <script src="{base_url}elements/assets/lib/bootstrap/js/bootstrap.min.js"></script>
    <script class="include" type="text/javascript" src="{base_url}elements/assets/lib/jquery.dcjqaccordion.2.7.js"></script>
    <script src="{base_url}elements/assets/lib/jquery.scrollTo.min.js"></script>
    <script src="{base_url}elements/assets/lib/jquery.nicescroll.js" type="text/javascript"></script>
    <script src="{base_url}elements/assets/lib/jquery.sparkline.js"></script>
    <!--common script for all pages-->
    <script src="{base_url}elements/assets/lib/common-scripts.js"></script>
    <script type="text/javascript" src="{base_url}elements/assets/lib/gritter/js/jquery.gritter.js"></script>
    <script type="text/javascript" src="{base_url}elements/assets/lib/gritter-conf.js"></script>
    <!--script for this page-->
    <script src="{base_url}elements/assets/lib/sparkline-chart.js"></script>
    <script src="{base_url}elements/assets/lib/zabuto_calendar.js"></script>
    </body>

    </html>
