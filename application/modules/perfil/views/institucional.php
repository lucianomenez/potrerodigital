<!DOCTYPE html>
<html lang="en">
{head}
<body>
  <section id="container">
    <!-- **********************************************************************************************************************************************************
        TOP BAR CONTENT & NOTIFICATIONS
        *********************************************************************************************************************************************************** -->
    <!--header start-->
    {header}
    <!--header end-->
    <!-- **********************************************************************************************************************************************************
        MAIN SIDEBAR MENU
        *********************************************************************************************************************************************************** -->
    <!--sidebar start-->
    {navbar}
    <!--sidebar end-->
    <!-- **********************************************************************************************************************************************************
        MAIN CONTENT
        *********************************************************************************************************************************************************** -->
    <!--main content start-->
    <section id="main-content">
      <section class="wrapper site-min-height">



        <!-- /container -->
      </section>
      <!-- /wrapper -->
    </section>
    <!-- js placed at the end of the document so the pages load faster -->
    <script src="{base_url}elements/assets/lib/jquery/jquery.min.js"></script>
    <script src="{base_url}elements/assets/lib/bootstrap/js/bootstrap.min.js"></script>
    <script class="include" type="text/javascript" src="{base_url}elements/assets/lib/jquery.dcjqaccordion.2.7.js"></script>
    <script src="{base_url}elements/assets/lib/jquery.scrollTo.min.js"></script>
    <script src="{base_url}elements/assets/lib/jquery.nicescroll.js" type="text/javascript"></script>
    <script src="{base_url}elements/assets/lib/jquery.sparkline.js"></script>
    <!--common script for all pages-->
    <script src="{base_url}elements/assets/lib/common-scripts.js"></script>
    <script type="text/javascript" src="{base_url}elements/assets/lib/gritter/js/jquery.gritter.js"></script>
    <script type="text/javascript" src="{base_url}elements/assets/lib/gritter-conf.js"></script>
    <!--script for this page-->
    <script src="{base_url}elements/assets/lib/sparkline-chart.js"></script>
    <script src="{base_url}elements/assets/lib/zabuto_calendar.js"></script>
    </body>

    </html>
