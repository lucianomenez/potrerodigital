<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="Dashboard">
  <meta name="keyword" content="Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">
  <title>Potrero Digital - Plataforma Virtual</title>

  <!-- Favicons -->
  <link href="{base_url}elements/assets/img/favicon.png" rel="icon">
  <link href="{base_url}elements/assets/img/apple-touch-icon.png" rel="apple-touch-icon">

  <!-- Bootstrap core CSS -->
  <link href="{base_url}elements/assets/lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <!--external css-->
  <link href="{base_url}elements/assets/lib/font-awesome/css/font-awesome.css" rel="stylesheet" />
  <!-- Custom styles for this template -->
  <link href="{base_url}elements/assets/css/style.css" rel="stylesheet">
  <link href="{base_url}elements/assets/css/style-responsive.css" rel="stylesheet">

  <!-- =======================================================
    Template Name: Dashio
    Template URL: https://templatemag.com/dashio-bootstrap-admin-template/
    Author: TemplateMag.com
    License: https://templatemag.com/license/
  ======================================================= -->
</head>

<body>
  <!-- **********************************************************************************************************************************************************
      MAIN CONTENT
      *********************************************************************************************************************************************************** -->
  <div class="container">
    <div class="row">
      <div class="col-lg-6 col-lg-offset-3 p404 centered">
        <h1 class="mt4">Gracias {nombre}!</h1>
        <h2 class="mt4">Tu solicitud fue recibida con éxito!</h2>
        <h3>A la brevedad el Equipo de Potrero se estará comunicando con vos.</h3>
        <br>
        <h5 class="mt"></h5>
        <p><a href="{base_url}user/login">Index</a> | <a href="https://www.potrerodigital.org">Ir a la Página de Potrero Digital</a>  </p>
      </div>
    </div>
  </div>
  <!-- js placed at the end of the document so the pages load faster -->
  <script src="{base_url}elements/assets/lib/jquery/jquery.min.js"></script>
  <script src="{base_url}elements/assets/lib/bootstrap/js/bootstrap.min.js"></script>
</body>

</html>
