<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
/*
 * Paneles de Navegación
 *
 * @author Héctor Gabriel Alegre <gabriel.h.alegre@gmail.com>
 */
class Panel extends MX_Controller {

    function __construct() {
        parent::__construct();
          $this->base_url = base_url();
          $this->module_url = base_url() . $this->router->fetch_module() . '/';
          $this->user->authorize();
          $this->load->helper('url');
          $this->load->model('app');
          $this->load->model('user/user');
          $this->load->config('config');
          $this->idu = $this->user->idu;
        }

        function index(){
          $data['base_url'] = $this->base_url;
          $data['module_url'] = $this->module_url;
          Modules::run('dashboard/dashboard', 'panel/json/panel.json',$debug, $data);
        }
        
        function content(){
          $data['base_url'] = $this->base_url;
          $data['module_url'] = $this->module_url;

          echo $this->load->view('index', $data);
        }
        
        function left_navbar(){
          $data['base_url'] = $this->base_url;
          $data['module_url'] = $this->module_url;

          echo $this->load->view('left_content', $data);
        }
        
        function get($id=null,$array = false){
          $url = "https://docs.google.com/spreadsheets/d/e/2PACX-1vSHMJUxRIOmlCpD59UYwCKVOVsaZWOQ0iAF8_ghnYwD9R2U7rB7ks88lJYhioY8B81egbCPvhzPUn_D/pub?output=csv";
          $data = file_get_contents($url);
          $rows = explode("\n",$data);
          $s = array();
          foreach($rows as $row) {
              $s[] = str_getcsv($row);
          }
          var_dump($s);exit;

          
          if (($handle = fopen($url, "r")) !== FALSE) { 
              while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) { 
               $num = count($data); 
               echo "<p> $num fields in line $row: <br /></p>\n"; 
               $row++; 
               for ($c=0; $c < $num; $c++) { 
                echo $data[$c] . "<br />\n"; 
               } 
              } 
              fclose($handle); 
          } 
 
        }

}