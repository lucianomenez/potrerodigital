main_component = {
  props: [],
  data: function () {
    return {
      modules: {},
      certificados: [],
    };
  },
  methods: {
    axiosData(url,callback) {
      axios
        .get(window.location.origin+'/'+url)
        .then((response) => {
          if (response.status == 200) {
            if (callback) {
              callback(response.data);
            }
          }
        })
        .catch((error) => {
          console.log(error);
        })
        .then(function () {});
    },
  },
  created() {
    AppBus.$on("change_module", ($event) => {
      this.modules = {};
      if ($event == "certificados_listado") {
        let obj=this;
        obj.certificados=[];
        this.axiosData('certificados/get_alumnos_by_group',function(data) {
          console.log(data)
          obj.certificados=data;
         });
      }
      this.modules[$event] = true;
    });
  },
};
