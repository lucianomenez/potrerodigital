{ignore}
<v-container fluid v-if="modules.carga">
    <v-row justify="center">
        <v-col cols="12">
            <v-col cols="12" class="b-b py-0">
                <div class="font-weight-regular text-h5">
                    Certificados
                </div>
            </v-col>
        </v-col>
        <v-col cols="12" md="5">
            <file-agent name_element="xls_alumnos" folder="xls_alumnos" theme="list" max_files="1" label="Cargar xls"
                id="home" max_size="2MB">
            </file-agent>
        </v-col>
    </v-row>
</v-container>

<v-container fluid v-if="modules.certificados_listado">
    <v-row justify="center">
        <v-col cols="12">
            <v-col cols="12" class="b-b py-0">
                <div class="font-weight-regular text-h5">
                    Certificados
                </div>
            </v-col>
        </v-col>
        <v-col cols="12" md="12" >
            <div class="font-weight-regular text-h6 pl-4">
                2020
            </div>
            <v-col cols="12" md="10" class="mx-auto">
                <v-expansion-panels popout>
                    <v-expansion-panel v-for="(element, i) in certificados" :key="i">
                        <v-expansion-panel-header>
                            {{element.origen}}
                        </v-expansion-panel-header>
                        <v-expansion-panel-content>
                            <span v-for="(alumno, idnumber) in element.alumnos">{{alumno.lastname}}</span>
                        </v-expansion-panel-content>
                    </v-expansion-panel>
                </v-expansion-panels>
            </v-col>
        </v-col>
    </v-row>
</v-container>
{/ignore}