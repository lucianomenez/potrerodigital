var base_url = $("#base_url").val();

$('#send_comment').on('click', function(e){
  var idu = $("#idu").val();
  var texto = $("#comment").val();
  var id = $(this).attr("data-idcurso");
  var base_url = $("#base_url").val();

  if (texto != ""){
    $.ajax({
     url : base_url + 'alumno/api/insert_comment',
     type: 'POST',
     data: {
       idu : idu,
       texto : texto,
       id_carrera : id
     },
     success: function(data){
       location.reload();
     }
    });
  }else{
    alert ("No podés enviar un mensaje vacío");
  }
});

$('.send_inscripcion').on('click', function(e){
  var idu = $("#idu").val();
  var id = $(this).attr("data-idcarrera");
  var base_url = $("#base_url").val();
  alert("Solicitud Enviada!");
  $.ajax({
   url : base_url + 'alumno/api/send_inscripcion',
   type: 'POST',
   data: {
     idu : idu,
     id : id
   },
   success: function(data){
     location.reload();
   }
  });
});

$('.subir_tarea').on('click', function(e){
  var idu = $("#idu").val();
  var id = $(this).attr("data-idtarea");
  var base_url = $("#base_url").val();
  $('#Modal_adjuntar_tarea').modal('show');

});

$('.ver_clase').on('click', function(e){
  var id = $(this).attr("data-idclase");
  id = parseInt(id);
  $.ajax({
   url : base_url + 'docente/api/get_clase',
   type: 'POST',
   data: {
     id : id
   },
   success: function(data){
     response = JSON.parse(data);
     console.log(response);
     $("#nombre_clase").html(response.nombre_clase);
     $("#sintesis").html(response.sintesis);
     $("#desarrollo").html(response.desarrollo);
     $("#objetivos").html(response.objetivos);
   }
  });

    $('#Modal_ver_clase').modal('show');
});

$('.update_tarea').on('change', function(e){
  var id = $(this).val();
  var status = $(this).is(':checked');
  var base_url = $("#base_url").val();
  alert("El estado de la tarea fue actualizado!");
  $.ajax({
   url : base_url + 'alumno/api/update_tarea',
   type: 'POST',
   data: {
     id : id,
     status : status
   },
   success: function(data){
     location.reload();
   }
  });
});

$('.modal_trigger').on('click', function(e){
  var id = $(this).attr("data-idcurso");
  $("#id_curso").val(id);
});


$('.delete_clase').on('click', function(e){
  var id = $(this).attr("data-idclase");
  e.preventDefault();
  if (window.confirm("Estás seguro que querés borrar ésta clase?")) {
    $.ajax({
     url : base_url + 'docente/api/borrar_clase',
     type: 'POST',
     data: {
       id : id
     },
     success: function(data){
       location.reload();
     }
    });
  }

})

$('.modal_edit').on('click', function(e){
  var id = $(this).attr("data-idclase");
  id = parseInt(id);
  $.ajax({
   url : base_url + 'docente/api/get_clase',
   type: 'POST',
   data: {
     id : id
   },
   success: function(data){
     response = JSON.parse(data);
     $("#id_curso_edit").val(response.id_curso);
     $("#nombre_clase").val(response.nombre_clase);
     $("#sintesis").val(response.sintesis);
     $("#desarrollo").val(response.desarrollo);
     $("#link_externo").val(response.link_externo);
     $("#objetivos").val(response.objetivos);
     $("#id_clase").val(response.id_clase);
   }
  });

$('#Modal_edit').modal('show');

});
