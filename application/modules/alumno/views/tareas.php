<!DOCTYPE html>
<html lang="en">
{head}
<link rel="stylesheet" href="{base_url}elements/assets/css/to-do.css">

<body>
  <input type="hidden" id="idu" name="idu" value="{idu}">
  <input type="hidden" id="base_url" name="base_url" value="{base_url}">
  <section id="container">
    <!-- **********************************************************************************************************************************************************
        TOP BAR CONTENT & NOTIFICATIONS
        *********************************************************************************************************************************************************** -->
    <!--header start-->
    {header}
    <!--header end-->
    <!-- **********************************************************************************************************************************************************
        MAIN SIDEBAR MENU
        *********************************************************************************************************************************************************** -->
    <!--sidebar start-->
    {navbar}
    <!--sidebar end-->
    <!-- **********************************************************************************************************************************************************
        MAIN CONTENT
        *********************************************************************************************************************************************************** -->
    <!--main content start-->
    <section id="main-content">
      <section class="wrapper">
        <h3><i class="fa fa-angle-right"></i> Mis Tareas</h3>
        <!-- SIMPLE TO DO LIST -->
        <!-- /row -->
        <!-- SORTABLE TO DO LIST -->
        <div class="row mt mb">
          <div class="col-md-12">
            <section class="task-panel tasks-widget">
              <div class="panel-heading">
                <div class="pull-left">
                  <h5><i class="fa fa-tasks"></i> Tareas Asignadas a mí</h5>
                </div>
                <br>
              </div>
              <div class="panel-body">
                <div class="task-content">
                  <ul id="sortable" class="task-list">
                    {tareas}
                    <li class="list-primary">
                      <i class=" fa fa-ellipsis-v"></i>
                      <div class="task-checkbox">
                        <input type="checkbox" {checked} class="list-child update_tarea" value="{id}" />
                      </div>
                      <div class="task-title">
                        <span class="task-title-sp">{nombre_tarea}</span>
                        <span class="badge bg-{class}">{estado}</span>
                        <div class="pull-right hidden-phone">
                          <button class="btn btn-success btn-xs fa fa-upload subir_tarea"></button>
                          <a href="{archivo_adjunto}"><button class="btn btn-primary btn-xs fa fa-file"></button></a>
                          <a href="{link_externo}"><button class="btn btn-warning btn-xs fa fa-link"></button></a>
                        </div>
                      </div>
                    </li>
                    {/tareas}
                  </ul>
                </div>
              </div>
            </section>
          </div>
          <!--/col-md-12 -->
        </div>
        <!-- /row -->
      </section>
      <!-- /wrapper -->
    </section>

    <div class="modal fade bd-example-modal-lg" id="Modal_adjuntar_tarea" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title">Adjuntar la tarea resuelta</h4>
          </div>
          <div class="modal-body">
            <form role="form" class="form-horizontal" method="post" action="{base_url}alumno/api/adjuntar_tarea"  enctype="multipart/form-data">
              <div class="form-group">
                <label class="col-lg-2 control-label">Archivo de la tarea</label>
                <div class="col-lg-6">
                  <input type="file" name="tarea" id="exampleInputFile" class="file-pos">
                </div>
              </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Enviar</button>
          </form>
          </div>
        </div>
      </div>
    </div>
    <!--main content end-->
    <!--footer start-->
    <!--footer end-->
  </section>
  <!-- js placed at the end of the document so the pages load faster -->
  <script src="{base_url}elements/assets/lib/jquery/jquery.min.js"></script>
  <script src="{base_url}elements/assets/lib/bootstrap/js/bootstrap.min.js"></script>
  <script class="include" type="text/javascript" src="{base_url}elements/assets/lib/jquery.dcjqaccordion.2.7.js"></script>
  <script src="{base_url}elements/assets/lib/jquery.scrollTo.min.js"></script>
  <script src="{base_url}elements/assets/lib/jquery.nicescroll.js" type="text/javascript"></script>
  <script src="{base_url}elements/assets/lib/jquery.sparkline.js"></script>
  <!--common script for all pages-->
  <script src="{base_url}elements/assets/lib/common-scripts.js"></script>
  <script type="text/javascript" src="{base_url}elements/assets/lib/gritter/js/jquery.gritter.js"></script>
  <script type="text/javascript" src="{base_url}elements/assets/lib/gritter-conf.js"></script>
  <!--script for this page-->
  <script src="{base_url}elements/assets/lib/sparkline-chart.js"></script>
  <script src="{base_url}elements/assets/lib/zabuto_calendar.js"></script>
  <script src="{base_url}alumno/assets/js/app.js"></script>
</body>

</html>
