<!DOCTYPE html>
<html lang="en">
{head}
<body>
  <input type="hidden" id="idu" name="idu" value="{idu}">
  <input type="hidden" id="base_url" name="base_url" value="{base_url}">
  <section id="container">
    <!-- **********************************************************************************************************************************************************
        TOP BAR CONTENT & NOTIFICATIONS
        *********************************************************************************************************************************************************** -->
    <!--header start-->
    {header}
    <!--header end-->
    <!-- **********************************************************************************************************************************************************
        MAIN SIDEBAR MENU
        *********************************************************************************************************************************************************** -->
    <!--sidebar start-->
    {navbar}
    <!--sidebar end-->
    <!-- **********************************************************************************************************************************************************
        MAIN CONTENT
        *********************************************************************************************************************************************************** -->
    <!--main content start-->
    <section id="main-content">
      <section class="wrapper site-min-height">
        <div class="row">
          <div class="col-lg-12">
            <!-- end col-4 -->
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
              <div class="custom-box">
                <div class="servicetitle">
                  <h4>Especialización Programación - Trayecto Full Stack Developer</h4>
                  <hr>
                </div>
                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry.</p>
                <ul class="pricing">
                  <li>4 Cuatrimestres en total</li>
                  <li>Cursada de 3 veces por semana presenciales</li>
                  <li>Curso Complementario de Inglés</li>
                  <li>Curso Complementario de Habilidades Interpersonales</li>
                  <li>Encuentros semanales online</li>
                  <li>Disponible en modalidad semi-presencial</li>
                  <li>Ayuda On-line 24/7</li>
                </ul>
                  {button_programacion}
              </div>
              <!-- end custombox -->
            </div>
            <!-- end col-4 -->
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
              <div class="custom-box">
                <div class="servicetitle">
                  <h4>Especialización Marketing Digital - Trayecto Analista de Marketing Digital </h4>
                  <hr>
                </div>
                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry.</p>
                <ul class="pricing">
                  <li>4 Cuatrimestres en total</li>
                  <li>Cursada de 3 veces por semana presenciales</li>
                  <li>Curso Complementario de Inglés</li>
                  <li>Curso Complementario de Habilidades Interpersonales</li>
                  <li>Encuentros semanales online</li>
                  <li>Disponible en modalidad semi-presencial</li>
                  <li>Ayuda On-line 24/7</li>
                </ul>
                {button_marketing}
              </div>
              <!-- end custombox -->
            </div>
            <!-- end col-4 -->
          </div>
          <!--  /col-lg-12 -->
        </div>

        <div class="row">
          <div class="col-lg-12">
            <!-- end col-4 -->
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
              <div class="custom-box">
                <div class="servicetitle">
                  <h3>Reglamento</h3>
                  <hr>
                </div>
                <ul class="pricing">
                  <li><h3>Herramientas informáticas e internet</h3><p>Nivel de dominio de intermedio a avanzado</p></li>
                  <li><h3>Redes sociales y programación inicial</h3><p>Nivel de dominio de intermedio a avanzado</p></li>
                  <li><h3>Trabajar en el mundo digital</h3><p>Ya sea a través de proyectos o emprendimientos propios, mediante la prestación de servicios o la búsqueda de empleo en empresas de tecnología o de la denominada economía digital</p></li>
                  <li><h3>Inscripción</h3><p>Completar formulario de inscripción y estar comprendido en las condiciones de la Beca de Honor que se otorga a cada alumno (según desempeño y de renovación cuatrimestral)</p></li>
                  <li><h3>Asistir</h3><p>Participar de las reuniones informativas, clases abiertas y entrevistas con autoridades</p></li>
                  <li><h3>Admisión</h3><p>Aceptar el código de convivencia en caso de ser admitido</p></li>
                </ul>
              </div>
              <!-- end custombox -->
            </div>
            <!-- end col-4 -->
            <!-- end col-4 -->
          </div>
          <!--  /col-lg-12 -->
        </div>
      <!--  /row -->
      </section>
      <!-- /wrapper -->
    </section>
    <!--main content end-->
    <!--footer start-->
    <!--footer end-->
  </section>
  <!-- js placed at the end of the document so the pages load faster -->
  <script src="{base_url}elements/assets/lib/jquery/jquery.min.js"></script>
  <script src="{base_url}elements/assets/lib/bootstrap/js/bootstrap.min.js"></script>
  <script class="include" type="text/javascript" src="{base_url}elements/assets/lib/jquery.dcjqaccordion.2.7.js"></script>
  <script src="{base_url}elements/assets/lib/jquery.scrollTo.min.js"></script>
  <script src="{base_url}elements/assets/lib/jquery.nicescroll.js" type="text/javascript"></script>
  <script src="{base_url}elements/assets/lib/jquery.sparkline.js"></script>
  <!--common script for all pages-->
  <script src="{base_url}elements/assets/lib/common-scripts.js"></script>
  <script type="text/javascript" src="{base_url}elements/assets/lib/gritter/js/jquery.gritter.js"></script>
  <script type="text/javascript" src="{base_url}elements/assets/lib/gritter-conf.js"></script>
  <!--script for this page-->
  <script src="{base_url}elements/assets/lib/sparkline-chart.js"></script>
  <script src="{base_url}elements/assets/lib/zabuto_calendar.js"></script>
  <script src="{base_url}alumno/assets/js/app.js"></script>

</body>

</html>
