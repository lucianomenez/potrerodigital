<!DOCTYPE html>
<html lang="en">
{head}
<link rel="stylesheet" href="{base_url}elements/assets/css/to-do.css">

<body>
  <section id="container">
    <input type="hidden" id="idu" name="idu" value="{idu}">
    <input type="hidden" id="base_url" name="base_url" value="{base_url}">
    <!-- **********************************************************************************************************************************************************
        TOP BAR CONTENT & NOTIFICATIONS
        *********************************************************************************************************************************************************** -->
    <!--header start-->
    {header}
    <!--header end-->
    <!-- **********************************************************************************************************************************************************
        MAIN SIDEBAR MENU
        *********************************************************************************************************************************************************** -->
    <!--sidebar start-->
    {navbar}
    <!--sidebar end-->
    <!-- **********************************************************************************************************************************************************
        MAIN CONTENT
        *********************************************************************************************************************************************************** -->
    <!--main content start-->

    <section id="main-content">
      <section class="wrapper">
        <h3><i class="fa fa-angle-right"></i> Cursos Disponibles</h3>
        <!-- COMPLEX TO DO LIST -->
        {cursos}
        <div class="row mt">
          <div class="col-md-12">
            <section class="task-panel tasks-widget">
              <div class="panel-heading">
                <div class="pull-left">
                  <h5><i class="fa fa-tasks"></i>{nombre_curso}</h5>
                </div>
                <br>
              </div>
              <div class="panel-body">
                <div class="task-content">
                  <ul class="task-list">
                    {clases}
                    <li>
                      <div class="task-checkbox">
                        <input type="checkbox" class="list-child" value="" />
                      </div>
                      <div class="task-title">
                        <span class="task-title-sp">{nombre_clase}</span>
                        <span class="badge bg-theme"></span>
                        <div class="pull-right hidden-phone">
                          <a href="{base_url}{archivo_adjunto}"><button class="btn btn-success btn-xs"><i class=" fa fa-download"></i></button></a>
                          <button class="btn btn-primary btn-xs ver_clase" data-idclase="{id_clase}"><i class="fa fa-eye"></i></button>
                          <a href="{link_externo}"><button class="btn btn-danger btn-xs"><i class="fa fa-link"></i></button></a>
                        </div>
                      </div>
                    </li>
                    {/clases}
                  </ul>
                </div>
              </div>
            </section>
          </div>
          <!-- /col-md-12-->
        </div>
        {/cursos}
        <!-- /row -->
      </section>
      <!-- /wrapper -->
    </section>
    <!--main content end-->
    <!--footer start-->
    <!--footer end-->
    <div class="modal fade bd-example-modal-lg" id="Modal_ver_clase" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title" id="nombre_clase">Resumen de la clase</h4>
          </div>
          <div class="modal-body" id="sintesis">
          </div>
          <div class="modal-body" id="desarrollo">
          </div>
          <div class="modal-body" id="objetivo">
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
          </div>
        </div>
      </div>
    </div>


  </section>

  <!-- js placed at the end of the document so the pages load faster -->
  <script src="{base_url}elements/assets/lib/jquery/jquery.min.js"></script>
  <script src="{base_url}elements/assets/lib/bootstrap/js/bootstrap.min.js"></script>
  <script class="include" type="text/javascript" src="{base_url}elements/assets/lib/jquery.dcjqaccordion.2.7.js"></script>
  <script src="{base_url}elements/assets/lib/jquery.scrollTo.min.js"></script>
  <script src="{base_url}elements/assets/lib/jquery.nicescroll.js" type="text/javascript"></script>
  <script src="{base_url}elements/assets/lib/jquery.sparkline.js"></script>
  <!--common script for all pages-->
  <script src="{base_url}elements/assets/lib/common-scripts.js"></script>
  <script type="text/javascript" src="{base_url}elements/assets/lib/gritter/js/jquery.gritter.js"></script>
  <script type="text/javascript" src="{base_url}elements/assets/lib/gritter-conf.js"></script>
  <!--script for this page-->
  <script src="{base_url}elements/assets/lib/sparkline-chart.js"></script>
  <script src="{base_url}alumno/assets/js/app.js"></script>
</body>

</html>
