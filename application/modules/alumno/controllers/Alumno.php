<?php

class Alumno extends MX_Controller {

  function __construct() {
      parent::__construct();
      $this->base_url = base_url();
      $this->module_url = base_url() . $this->router->fetch_module() . '/';
      $this->user->authorize();
      $this->load->module('elements');
      $this->load->helper('file');
      $this->load->helper('url');
      $this->load->library('parser');
      $this->load->model('app');
      $this->load->model('user/user');
      $this->load->model('Model_alumno');
      $this->load->model('docente/Model_docente');
      $this->idu = $this->user->idu;
      $this->alumno = $this->user->get_user($this->idu);
      error_reporting(0);

  }

    //==== MAIN ALUMNO

    function Index() {
      $data["base_url"] = $this->base_url;
      $data["module_url"] = $this->module_url;
      $data["idu"] = $this->idu;
      $data["head"] = $this->elements->head();
      $data["header"] = $this->elements->ui_header();
      $data["navbar"] = $this->elements->ui_navbar();
      $data["cursos"] = $this->Model_alumno->get_cursos();
      echo $this->parser->parse("index", $data, true, true);
     	//$customData['usercan_create']=true;
    }


    function material_de_estudio() {
      $data["base_url"] = $this->base_url;
      $data["module_url"] = $this->module_url;
      $data["idu"] = $this->idu;
      $data["head"] = $this->elements->head();
      $data["header"] = $this->elements->ui_header();
      $data["navbar"] = $this->elements->ui_navbar();
      $data["items"] = $this->Model_alumno->get_material();      
      echo $this->parser->parse("material_de_estudio", $data, true, true);
     	//$customData['usercan_create']=true;
    }


    function cursos() {
      $data["base_url"] = $this->base_url;
      $data["module_url"] = $this->module_url;
      $data["idu"] = $this->idu;
      $data["header"]= $this->elements->ui_header();
      $data["navbar"]= $this->elements->ui_navbar();
      $data["head"]= $this->elements->head();
      $idcurso = substr($this->alumno->curso,-1) ;
      $data["cursos"] = $this->Model_alumno->get_cursos($idcurso);

      foreach ($data['cursos'] as &$curso){
        $curso['clases'] = $this->Model_docente->get_clases_por_curso($curso["id"]);
      };
      echo $this->parser->parse("cursos", $data, true, true);
     	//$customData['usercan_create']=true;
    }

    function carreras() {
      $data["base_url"] = $this->base_url;
      $data["module_url"] = $this->module_url;
      $data["idu"] = $this->idu;
      $data["header"]= $this->elements->ui_header();
      $data["navbar"]= $this->elements->ui_navbar();
      $data["head"]= $this->elements->head();

      $data["button_programacion"] = '<a class="btn btn-theme send_inscripcion" data-idcarrera="1">Solicitar Inscripción</a>';
      $data["button_marketing"] = '<a class="btn btn-theme send_inscripcion" data-idcarrera="2">Solicitar Inscripción</a>';

      $data["carreras"] = $this->Model_alumno->get_inscripciones(strval($this->idu));
      if (!empty($data["carreras"])){
        foreach ($data["carreras"] as $carrera){
          if ($carrera["id"] == "1"){
            switch ($carrera["estado"]) {
                case "aprobada":
                    $data["button_programacion"] = '<a class="btn btn-success">Aprobada</a>';
                    break;
                case "pendiente":
                    $data["button_programacion"] = '<a class="btn btn-warning">Pendiente de Aprobación</a>';
                    break;
                case "rechazada":
                    $data["button_programacion"] = '<a class="btn btn-danger">Rechazada</a>';
                    break;
                default:
                    $data["button_programacion"] = '<a class="btn btn-theme send_inscripcion" data-idcarrera="1">Solicitar Inscripción</a>';
            }
          }
          if ($carrera["id"] == "2"){
            switch ($carrera["estado"]) {
                case "aprobada":
                    $data["button_marketing"] = '<a class="btn btn-success">Aprobada</a>';
                    break;
                case "pendiente":
                    $data["button_marketing"] = '<a class="btn btn-warning">Pendiente de Aprobación</a>';
                    break;
                case "rechazada":
                    $data["button_marketing"] = '<a class="btn btn-danger">Rechazada</a>';
                    break;
                default:
                    $data["button_marketing"] = '<a class="btn btn-theme send_inscripcion" data-idcarrera="2">Solicitar Inscripción</a>';
            }
          }

        }

      }else{
        $data["button_programacion"] = '<a class="btn btn-theme send_inscripcion" data-idcarrera="1">Solicitar Inscripción</a>';
        $data["button_marketing"] = '<a class="btn btn-theme send_inscripcion" data-idcarrera="2">Solicitar Inscripción</a>';
      }


      echo $this->parser->parse("carreras", $data, true, true);
     	//$customData['usercan_create']=true;
    }


    function tareas() {
      $data["base_url"] = $this->base_url;
      $data["module_url"] = $this->module_url;
      $data["idu"] = $this->idu;
      $data["header"]= $this->elements->ui_header();
      $data["navbar"]= $this->elements->ui_navbar();
      $data["head"]= $this->elements->head();
      $data["tareas"] = $this->Model_alumno->get_tareas($this->idu);
      foreach ($data["tareas"] as $tarea){
        if ($tarea['estado'] == "Pendiente"){
          $data["class"] = 'warning';
          $data["checked"] = '';
        }else{
          $data["class"] = 'success';
          $data["checked"] = 'checked';
        }
      }

      echo $this->parser->parse("tareas", $data, true, true);
     	//$customData['usercan_create']=true;
    }






} //

?>
