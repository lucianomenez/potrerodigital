<?php

class Api extends MX_Controller {

  function __construct() {
      parent::__construct();
      $this->base_url = base_url();
      $this->module_url = base_url() . $this->router->fetch_module() . '/';
      $this->user->authorize();
      $this->load->module('elements');
      $this->load->helper('file');
      $this->load->helper('url');
      $this->load->library('parser');
      $this->load->model('app');
      $this->load->model('user/user');
      $this->load->model('Model_alumno');
      $this->load->model('foros/Model_foro');

      $this->idu = $this->user->idu;
      $this->alumno = $this->user->get_user($this->idu);

  }

    //==== MAIN ALUMNO

    function insert_comment() {
      $data = $this->input->post();
      $data["fecha"] = date("d-m-Y");
      $data["nombre"] = $this->alumno->name." ".$this->alumno->lastname;
      $foro = $this->Model_foro->get_foros($data["id"]);
      $foro[0]['mensajes'][] = $data;
      $this->Model_foro->update_foro($foro[0]);
      return;

     	//$customData['usercan_create']=true;
    }

    function send_inscripcion() {
      $data = $this->input->post();
      $data['estado'] = "pendiente";
      $data["name"] = $this->alumno->name;
      $data["lastname"] = $this->alumno->lastname;
      $data["email"] = $this->alumno->email;
      $this->Model_alumno->insert_inscripcion($data);
     	return;
    }

    function update_pass(){

      $data['usuarios'] = $this->user->get_users();
      foreach ($data['usuarios'] as $usuario){
        $usuario['passw'] = $this->hash($usuario['idnumber']);
        $this->user->update($usuario);
        echo "ok! <br/>";
      }
    }

    function hash($str) {
        return password_hash($str, PASSWORD_DEFAULT);
    }



    function update_tarea() {
      $data = $this->input->post();
      $tarea = $this->Model_alumno->get_tarea_por_id(intval($data["id"]));
      if ($data["status"] == "true"){
        $tarea["estado"] = "Resuelta";

      }else{
        $tarea["estado"] = "Pendiente";
      }
      $this->Model_alumno->update_tarea($tarea);
     	return;
    }






} //

?>
