<?php

/**
 * Funciones para el manejo de datos del container inscripciones
 * @author Luciano Menez <lucianomenez1212@gmail.com>
 * @date 3/05/2016
 *
 */

class Model_alumno extends CI_Model {

    public function __construct() {
        // Call the Model constructor
        parent::__construct();
        $this->load->library('cimongo/cimongo', '', 'db');
    }

    function get_cursos($id = null){
        $result = array();
        $container = 'potrerodigital.cursos';
        if ($id != null){
          $query= array('id' => $id);
          $this->db->where($query);
          $result = $this->db->get($container)->result_array();
          return $result;
        }else{
          $result = $this->db->get($container)->result_array();
          return $result;
        }
    }

    function get_material($id = null){
        $result = array();
        $container = 'potrerodigital.material_de_estudio';
        if ($id != null){
          $query= array('id' => $id);
          $this->db->where($query);
          $result = $this->db->get($container)->result_array();
          return $result;
        }else{
          $result = $this->db->get($container)->result_array();
          return $result;
        }
    }

    function get_tareas($idu = null){
        $result = array();
        $container = 'potrerodigital.tareas';
        if ($idu != null){
          $query= array('idu_alumno' => $idu);
          $this->db->where($query);
          $result = $this->db->get($container)->result_array();
          return $result;
        }else{
          $result = $this->db->get($container)->result_array();
          return $result;
        }
    }

    function get_tarea_por_id($id){
        $result = array();
        $container = 'potrerodigital.tareas';
        $query= array('id' => $id);
        $this->db->where($query);
        $result = $this->db->get($container)->result_array();
        return $result[0];

    }

    function get_carreras($id = null){
        $result = array();
        $container = 'potrerodigital.carreras';
        if ($id != null){
          $query= array('id' => $data['id']);
          $this->db->where($query);
          $result = $this->db->get($container)->result_array();
          return $result[0];
        }else{
          $result = $this->db->get($container)->result_array();
          return $result;
        }
    }

    function get_inscripciones($idu = null){
        $result = array();
        $container = 'carreras.inscripciones';
        if ($idu != null){
        $query= array('idu' => $idu);
        $this->db->where($query);
        }
        $result = $this->db->get($container)->result_array();
        return $result;

    }

    function update_curso($data){
        $result = array();
        $container = 'potrerodigital.cursos';
        $query= array('id' => $data['id']);
        $this->db->where($query);
        $result = $this->db->update($container, $data);
        return $result;
    }

    function update_tarea($data){
        $result = array();
        $container = 'potrerodigital.tareas';
        $query= array('id' => intval($data['id']));
        $this->db->where($query);
        $result = $this->db->update($container, $data);
        return $result;
    }


    function insert_inscripcion($data){
      $container = 'carreras.inscripciones';
      $this->db->insert($container, $data);
      return;
    }


}
