<?php

class Elements extends MX_Controller {

  function __construct() {
      parent::__construct();
      $this->base_url = base_url();
      $this->module_url = base_url() . $this->router->fetch_module() . '/';
//      $this->user->authorize();
      $this->load->helper('file');
      $this->load->library('parser');
      $this->load->helper('url');
      $this->load->model('app');
      $this->load->model('user/user');
      $this->idu = $this->user->idu;
      $this->alumno = $this->user->get_user($this->idu);

  }

    //==== MAIN ALUMNO

    function ui_navbar() {
      $data["base_url"] =  base_url();
      $data["idu"] = $this->idu;
      $data["name"] = $this->alumno->name;
      $data["lastname"] = $this->alumno->lastname;
      return $this->parser->parse("elements/navbar", $data, true, true);
     	//$customData['usercan_create']=true;
    }

    function ui_navbar_coordinador() {
      $data["base_url"] =  base_url();
      $data["idu"] = $this->idu;
      $data["name"] = $this->alumno->name;
      $data["lastname"] = $this->alumno->lastname;
      return $this->parser->parse("elements/navbar_coordinador", $data, true, true);
     	//$customData['usercan_create']=true;
    }

    function ui_navbar_docente() {
      $data["base_url"] =  base_url();
      $data["idu"] = $this->idu;
      $data["name"] = $this->alumno->name;
      $data["lastname"] = $this->alumno->lastname;
      return $this->parser->parse("elements/navbar_docente", $data, true, true);
     	//$customData['usercan_create']=true;
    }


    function head(){
      $data["base_url"] = base_url();
      $data["idu"] = $this->idu;
      return $this->parser->parse("elements/head", $data, true, true);
     	//$customData['usercan_create']=true;
    }

    function ui_header() {
      $data["base_url"] =  base_url();
      $data["idu"] = $this->idu;
      return $this->parser->parse("elements/header", $data, true, true);
     	//$customData['usercan_create']=true;
    }



} //

?>
