<header class="header black-bg">
  <div class="sidebar-toggle-box">
    <div class="fa fa-bars tooltips" data-placement="right" data-original-title="Toggle Navigation"></div>
  </div>
  <!--logo start-->
  <a href="index.html" class="logo"><b>Potrero<span>Digital</span></b></a>
  <!--logo end-->
  <div class="nav notify-row" id="top_menu">
    <!--  notification start -->
    <ul class="nav top-menu">
      <!-- settings start -->
      <li class="dropdown">
        <a data-toggle="dropdown" class="dropdown-toggle" href="index.html#">
          <i class="fa fa-tasks"></i>
          <span class="badge bg-theme">0</span>
          </a>
        <ul class="dropdown-menu extended tasks-bar">
          <div class="notify-arrow notify-arrow-green"></div>
          <li>
            <p class="green">No tenés tareas pendientes!</p>
          </li>
          <!-- <li>
            <a href="index.html#">
              <div class="task-info">
                <div class="desc">Entrega de listas HTML</div>
                <div class="percent">40%</div>
              </div>
              <div class="progress progress-striped">
                <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 40%">
                  <span class="sr-only">40% Completado</span>
                </div>
              </div>
            </a>
          </li>
          <li>
            <a href="index.html#">
              <div class="task-info">
                <div class="desc">Funciones Javascript</div>
                <div class="percent">60%</div>
              </div>
              <div class="progress progress-striped">
                <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%">
                  <span class="sr-only">60% Complete (warning)</span>
                </div>
              </div>
            </a>
          </li> -->
          <li class="external">
            <a href="{base_url}alumno/tareas">Ver todas las tareas</a>
          </li>
        </ul>
      </li>
      <!-- settings end -->
      <!-- inbox dropdown start-->
      <li id="header_inbox_bar" class="dropdown">
        <a data-toggle="dropdown" class="dropdown-toggle" href="index.html#">
          <i class="fa fa-envelope-o"></i>
          <span class="badge bg-theme">1</span>
          </a>
        <ul class="dropdown-menu extended inbox">
          <div class="notify-arrow notify-arrow-green"></div>
          <li>
            <p class="green">Tenés 1 mensaje nuevo</p>
          </li>
          <li>
            <a href="index.html#">
              <span class="photo"><img alt="avatar" src="{base_url}elements/assets/img/marca_potrero2.png"></span>
              <span class="subject">
              <span class="from">Potrero Digital</span>
              <span class="time">Recién</span>
              </span>
              <span class="message">
              Bienvenido a Potrero Digital!
              </span>
              </a>
          </li>
          <li>
            <a href="{base_url}mensajes">Ver todos los mensajes</a>
          </li>
        </ul>
      </li>
      <!-- inbox dropdown end -->
      <!-- notification dropdown start-->
      <li id="header_notification_bar" class="dropdown">
        <a data-toggle="dropdown" class="dropdown-toggle" href="index.html#">
          <i class="fa fa-bell-o"></i>
          <span class="badge bg-warning">!</span>
          </a>
        <ul class="dropdown-menu extended notification">
          <div class="notify-arrow notify-arrow-yellow"></div>
          <li>
            <p class="yellow">Hay mensajes nuevos en el Foro</p>
          </li>
          <li>
            <a href="{base_url}foros">Ir al Foro</a>
          </li>
        </ul>
      </li>
      <!-- notification dropdown end -->
    </ul>
    <!--  notification end -->
  </div>
  <div class="top-menu">
    <ul class="nav pull-right top-menu">
      <li><a class="logout" href="{base_url}perfil">Mi Perfil</a></li>
      <li><a class="logout" href="{base_url}user/logout">Salir</a></li>

    </ul>
  </div>
</header>
