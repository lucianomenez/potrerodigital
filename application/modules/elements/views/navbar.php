<aside>
  <div id="sidebar" class="nav-collapse ">
    <!-- sidebar menu start-->
    <ul class="sidebar-menu" id="nav-accordion">
      <div class="profile-pic mb">
      <p class="centered"><a href="{base_url}perfil"><img src="{base_url}perfil/assets/images/{idu}/profile.jpg" onerror="this.onerror=null; this.src='{base_url}perfil/assets/images/avatar.jpg'" class="img-circle" width="80"></a></p>
    </div>
      <h5 class="centered">{name} {lastname}</h5>
      <li class="mt">
        <a class="active" href="{base_url}alumno">
          <i class="fa fa-dashboard"></i>
          <span>Mi Panel</span>
          </a>
      </li>
      <li>
        <a href="{base_url}alumno/material_de_estudio">
          <i class="fa fa-graduation-cap"></i>
          <span>Material de Estudio</span>
          </a>
      </li>
      <li>
        <a href="{base_url}alumno/cursos">
          <i class="fa fa-book"></i>
          <span>Mis cursos</span>
          </a>
      </li>
      <li>
        <a href="{base_url}alumno/tareas">
          <i class="fa fa-tasks"></i>
          <span>Mis Tareas</span>
          </a>
      </li>
      <li>
        <a href="{base_url}editor_de_codigo">
          <i class="fa fa-code"></i>
          <span>Editor</span>
          </a>
      </li>

      <li>
        <a href="{base_url}mensajes">
          <i class="fa fa-envelope"></i>
          <span>Mensajes</span>
          <span class="label label-theme pull-right mail-info">1</span>
          </a>
      </li>
      <li>
        <a href="{base_url}foros">
          <i class="fa fa-comments"></i>
          <span>Foro</span>
          <span class="label label-warning pull-right mail-info">7</span>
          </a>
      </li>
    </ul>
    <!-- sidebar menu end-->
  </div>
</aside>
