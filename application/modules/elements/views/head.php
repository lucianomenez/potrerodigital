
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="Dashboard">
  <meta name="keyword" content="Potrero Digital, Aula Virtual, Marketing Digital, Programación, Escuela de Oficios Digitales">
  <title>Potrero Digital - Plataforma </title>

  <!-- Favicons -->
  <link href="http://potrerodigital.org/img/favicon.png" rel="icon">
  <link href="{base_url}elements/assets/img/apple-touch-icon.png" rel="apple-touch-icon">

  <!-- Bootstrap core CSS -->
  <link href="{base_url}elements/assets/lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <!--external css-->
  <link href="{base_url}elements/assets/lib/font-awesome/css/font-awesome.css" rel="stylesheet" />
  <link rel="stylesheet" type="text/css" href="{base_url}elements/assets/css/zabuto_calendar.css">
  <link rel="stylesheet" type="text/css" href="{base_url}elements/assets/lib/gritter/css/jquery.gritter.css" />
  <!-- Custom styles for this template -->
  <link href="{base_url}elements/assets/css/style.css" rel="stylesheet">
  <link href="{base_url}elements/assets/css/style-responsive.css" rel="stylesheet">
  <script src="{base_url}elements/assets/lib/chart-master/Chart.js"></script>
</head>
