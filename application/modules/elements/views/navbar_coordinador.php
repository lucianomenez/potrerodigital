<aside>
  <div id="sidebar" class="nav-collapse ">
    <!-- sidebar menu start-->
    <ul class="sidebar-menu" id="nav-accordion">
      <div class="profile-pic">
      <p class="centered"><a href="{base_url}perfil"><img src="{base_url}perfil/assets/images/{idu}/profile.jpg" class="img-circle" width="80"></a></p>
    </div>
      <h5 class="centered">{name} {lastname}</h5>
      <li class="mt">
        <a class="active" href="{base_url}coordinador">
          <i class="fa fa-dashboard"></i>
          <span>Mi Panel</span>
          </a>
      </li>
      <li>
        <a href="{base_url}coordinador/cursos/1">
          <i class="fa fa-book"></i>
          <span>Programación Frontend (Comisión I)</span>
          </a>
      </li>
      <li>
        <a href="{base_url}coordinador/cursos/2">
          <i class="fa fa-book"></i>
          <span>Programación Frontend (Comisión II)</span>
          </a>
      </li>
      <li>
        <a href="{base_url}coordinador/cursos/3">
          <i class="fa fa-book"></i>
          <span>Programación Frontend (Comisión III)</span>
          </a>
      </li>
      <li>
        <a href="{base_url}mensajes">
          <i class="fa fa-envelope"></i>
          <span>Mensajes</span>
          <span class="label label-theme pull-right mail-info">1</span>
          </a>
      </li>
      <li>
        <a href="{base_url}foros">
          <i class="fa fa-comments"></i>
          <span>Foro</span>
          <span class="label label-warning pull-right mail-info">7</span>
          </a>
      </li>
    </ul>
    <!-- sidebar menu end-->
  </div>
</aside>
