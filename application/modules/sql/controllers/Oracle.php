<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Oracle extends MX_Controller {

    function __construct() {
        parent::__construct();
        phpinfo();

        date_default_timezone_set('UTC');
        //---base variables
        $this->base_url = base_url();
        $this->module_url = base_url() . $this->router->fetch_module() . '/';
        $this->load->library('parser');
        $this->idu = (int) $this->session->userdata('iduser');
        //---Output Profiler
        //$this->output->enable_profiler(TRUE);
        //error_reporting(E_ALL);
    }
    
   
    function Index(){

        $conn = oci_connect('hr', 'welcome', 'MYDB');
        if (!$conn) {
            $e = oci_error();
            trigger_error(htmlentities($e['message'], ENT_QUOTES), E_USER_ERROR);
        }
        
        $stid = oci_parse($conn, 'SELECT * FROM employees');
        oci_execute($stid);
        
        echo "<table border='1'>\n";
        while ($row = oci_fetch_array($stid, OCI_ASSOC+OCI_RETURN_NULLS)) {
            echo "<tr>\n";
            foreach ($row as $item) {
                echo "    <td>" . ($item !== null ? htmlentities($item, ENT_QUOTES) : "") . "</td>\n";
            }
            echo "</tr>\n";
        }
        echo "</table>\n";



    }
    
    //====== Print calendar 
    
    
  
    
}//class