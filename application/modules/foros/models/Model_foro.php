<?php

/**
 * Funciones para el manejo de datos del container inscripciones
 * @author Luciano Menez <lucianomenez1212@gmail.com>
 * @date 3/05/2016
 *
 */

class Model_foro extends CI_Model {

    public function __construct() {
        // Call the Model constructor
        parent::__construct();
        $this->load->library('cimongo/cimongo', '', 'db');
    }

    function get_foros($id = null){
        $result = array();
        $container = 'potrerodigital.foros';
        if ($id != null){
          $query= array('id' => $id);
          $this->db->where($query);
          $result = $this->db->get($container)->result_array();
          return $result;
        }else{
          $result = $this->db->get($container)->result_array();
          return $result;
        }
    }

    function update_foro($data){
        $result = array();
        $container = 'potrerodigital.foros';
        $query= array('id' => $data['id']);
        $this->db->where($query);
        $this->db->update($container, $data);
        return;
    }




}
