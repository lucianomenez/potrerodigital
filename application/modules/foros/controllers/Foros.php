<?php

class Foros extends MX_Controller {

  function __construct() {
      parent::__construct();
      $this->base_url = base_url();
      $this->module_url = base_url() . $this->router->fetch_module() . '/';
      $this->user->authorize();
      $this->load->module('elements');
      $this->load->helper('file');
      $this->load->helper('url');
      $this->load->library('parser');
      $this->load->model('Model_foro');
      $this->load->model('user/user');
      $this->idu = $this->user->idu;
      $this->alumno = $this->user->get_user($this->idu);

  }

    //==== MAIN ALUMNO

    function Index() {
      $data["base_url"] = $this->base_url;
      $data["module_url"] = $this->module_url;
      $data["idu"] = $this->idu;
      $data["head"] = $this->elements->head();
      $data["header"] = $this->elements->ui_header();
      $data["navbar"] = $this->elements->ui_navbar();
      $data["foros"] = $this->Model_foro->get_foros();
      return $this->parser->parse("foros", $data, false, false);
     	//$customData['usercan_create']=true;
    }

    function ver_foro($id) {
      $data["base_url"] = $this->base_url;
      $data["module_url"] = $this->module_url;
      $data["idu"] = $this->idu;
      $data["head"] = $this->elements->head();
      $data["header"] = $this->elements->ui_header();
      $data["navbar"] = $this->elements->ui_navbar();
      $data["foro"] = $this->Model_foro->get_foros($id);
      return $this->parser->parse("view_foro", $data, false, false);
     	//$customData['usercan_create']=true;
    }

} //
