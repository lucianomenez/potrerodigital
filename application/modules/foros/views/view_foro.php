<!DOCTYPE html>
<html lang="en">
{head}
<body>
  <input type="hidden" id="idu" name="idu" value="{idu}">
  <input type="hidden" id="base_url" name="base_url" value="{base_url}">
  <section id="container">
    <!-- **********************************************************************************************************************************************************
        TOP BAR CONTENT & NOTIFICATIONS
        *********************************************************************************************************************************************************** -->
    <!--header start-->
    {header}
    <!--header end-->
    <!-- **********************************************************************************************************************************************************
        MAIN SIDEBAR MENU
        *********************************************************************************************************************************************************** -->
    <!--sidebar start-->
    {navbar}
    <!--sidebar end-->
        <section id="main-content">
          <section class="wrapper site-min-height">
            {foro}
            <div class="chat-room mt">
              <aside class="mid-side">
                <div class="chat-room-head">
                  <h3>{nombre_foro}</h3>
                </div>
                {mensajes}
                <div class="group-rom">
                  <div class="first-part">{nombre}</div>
                  <div class="second-part">{texto}</div>
                  <div class="third-part">{fecha}</div>
                </div>
                {/mensajes}
                <div class="group-rom last-group">
                </div>
                <footer>
                  <div class="chat-txt">
                    <input type="text" id="comment" class="form-control">
                  </div>
                  <button class="btn btn-theme" data-idcurso="{id}" id="send_comment">Enviar</button>
                </footer>
              </aside>
              <aside class="right-side">
                <div class="user-head">
                </div>
                <div class="invite-row">
                  <h4 class="pull-left">Moderadores</h4>
                </div>
                <ul class="chat-available-user">
                  {moderadores}
                  <li>
                    <a href="{base_url}perfil/ver_perfil/{idu_moderador}">
                      {nombre}
                      <span class="text-muted">1h:02m</span>
                      </a>
                  </li>
                  {/moderadores}
                </ul>
              </aside>
            </div>
            <!-- page end-->
            {/foro}
          </section>
          <!-- /wrapper -->
        </section>
        <!-- /MAIN CONTENT -->
        <!--main content end-->
    <!--main content end-->
    <!--footer start-->
    <!--footer end-->
  </section>
  <!-- js placed at the end of the document so the pages load faster -->
  <script src="{base_url}elements/assets/lib/jquery/jquery.min.js"></script>
  <script src="{base_url}elements/assets/lib/bootstrap/js/bootstrap.min.js"></script>
  <script class="include" type="text/javascript" src="{base_url}elements/assets/lib/jquery.dcjqaccordion.2.7.js"></script>
  <script src="{base_url}elements/assets/lib/jquery.scrollTo.min.js"></script>
  <script src="{base_url}elements/assets/lib/jquery.nicescroll.js" type="text/javascript"></script>
  <script src="{base_url}elements/assets/lib/jquery.sparkline.js"></script>
  <!--common script for all pages-->
  <script src="{base_url}elements/assets/lib/common-scripts.js"></script>
  <script type="text/javascript" src="{base_url}elements/assets/lib/gritter/js/jquery.gritter.js"></script>
  <script type="text/javascript" src="{base_url}elements/assets/lib/gritter-conf.js"></script>
  <!--script for this page-->
  <script src="{base_url}elements/assets/lib/sparkline-chart.js"></script>
  <script src="{base_url}elements/assets/lib/zabuto_calendar.js"></script>
  <script src="{base_url}alumno/assets/js/app.js"></script>


</body>

</html>
