<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Parser_elements extends MX_Controller {

    function __construct() {
        parent::__construct();
        $this->load->config('dashboard/config');
        $this->load->library('parser');
        $this->load->model('user/user');

        //---base variables
        $this->base_url = base_url();
        $this->module_url = base_url() . $this->router->fetch_module() . '/';
        //$this->user->authorize();
        $this->idu = $this->user->idu;
    }

    function navbar_vuetify_v1() {
        $data['base_url'] = $this->base_url;
        echo $this->parser->parse('dashboard/_navbar_vuetify_v1', $data, true, true);
    }

    function footer_vuetify_v1() {
        $data['base_url'] = $this->base_url;
        echo $this->parser->parse('dashboard/_footer_vuetify_v1', $data, true, true);
    }
    
}