file_agent = {
  props: [
    "name_element",
    "folder",
    "theme",
    "max_files",
    "label",
    "id",
    "max_size",
  ],
  data: function () {
    return {
      ref: "files" + this.name_element,
      fileRecords: [],
      fileRecordsForUpload: [],
      base_url: window.location.origin + "/",
    };
  },
  methods: {
    filesSelected: function (fileRecordsNewlySelected) {
      var validFileRecords = fileRecordsNewlySelected.filter(
        (fileRecord) => !fileRecord.error
      );
      this.fileRecordsForUpload = this.fileRecordsForUpload.concat(
        validFileRecords
      );
      let refs = this.$refs;
      let objs = {
        obj: refs[this.ref],
        fileRecordsForUpload: this.fileRecordsForUpload,
        element: this.name_element,
      };
      AppBus.$emit("change_file_agent", objs);
    },
    onBeforeDelete: function (fileRecord) {
      let app = this;
      let obj = app.$refs[this.ref];
      obj.deleteFileRecord(fileRecord);
    },

    fileDeleted: function (fileRecord) {
      var i = this.fileRecordsForUpload.indexOf(fileRecord);
      let refs = this.$refs;
      if (i !== -1) {
        this.fileRecordsForUpload.splice(i, 1);

        let objs = {
          obj: refs[this.ref],
          fileRecordsForUpload: this.fileRecordsForUpload,
          element: this.name_element,
        };
        AppBus.$emit("change_file_agent", objs);
      } else {
        this.deleteUploadedFile(fileRecord, refs);
      }
    },

    deleteUploadedFile: function (fileRecord, refs) {
      var i = this.fileRecords.indexOf(fileRecord);
      let objs = {
        obj: refs[this.ref],
        folder: this.folder,
        element: this.name_element,
        index: i,
      };
      AppBus.$emit("delete_file_agent", objs);
    },

    load_files: function (endpoint, element, id) {
      let obj = this;
      if (id != "nuevo_producto") {
        this.get_data(
          endpoint,
          function (data) {
            obj.fileRecords = data[0][element];
          },
          id
        );
      } else {
        (this.fileRecordsForUpload = []), (this.fileRecords = []);
      }
    },
    get_data(endpoint, callback, id) {
      axios
        .get(this.base_url + "website/api/get_" + endpoint + "/" + id)
        .then((response) => {
          if (response.status == 200) {
            if (callback) {
              callback(response.data);
            }
          }
        })
        .catch((error) => {})
        .then(function () {});
    },
  },
  created() {
    let objs = {
      element: this.name_element,
      folder: this.folder,
    };
    AppBus.$emit("create_file_agent", objs),
      AppBus.$on("clean_fileRecordsForUpload" + this.name_element, ($event) => {
        this.fileRecordsForUpload = [];
      });

    //this.load_files(this.folder, this.name_element, this.id);

    AppBus.$on("change_producto_activo", ($event) => {
      if ($event == this.id) {
        this.load_files(this.folder, this.name_element, this.id);
      }
    });
  },
};
