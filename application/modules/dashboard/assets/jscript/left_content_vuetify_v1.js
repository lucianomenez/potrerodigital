left_component = {
  props: [],
  data: function () {
    return { drawer: false, group: null };
  },

  methods: {
    change_module(module) {
      AppBus.$emit("change_module", module);
    },
  },
  created() {
    AppBus.$on("change_drawer", ($event) => {
      this.drawer = $event;
    });
  },
};
