top_component = {
  props: [],
  data: function () {
    return { drawer: false, base_url: window.location.origin + "/" };
  },
  watch: {
    drawer(val, prev) {
      AppBus.$emit("change_drawer", val);
    },
  },
  methods: {},
  created() {
    if (this.$vuetify.breakpoint.mdAndUp) {
      this.drawer = true;
    }
  },
};
