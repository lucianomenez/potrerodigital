const AppBus = new Vue();

file_agent.template = "#file-agent";
Vue.component("file-agent", file_agent);

top_component.template = "#top_navbar";
Vue.component("top-navbar", top_component);

left_component.template = "#left_navbar";
Vue.component("left-navbar", left_component);

main_component.template = "#main_content";
Vue.component("main-content", main_component);

new Vue({
  el: "#app",
  vuetify: new Vuetify(),
  data() {
    return {};
  },
  watch: {},
  computed: {},
  methods: {},
  created() {},
});
