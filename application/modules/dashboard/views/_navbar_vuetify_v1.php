<v-app-bar app light flat color="white">
    <v-app-bar-nav-icon @click="drawer = !drawer"></v-app-bar-nav-icon>
    <v-btn depressed text tile>
        <v-img :src="base_url+'dashboard/assets/img/logo.png'"
            :max-width="$vuetify.breakpoint.smAndDown ? '125' : '240'">
        </v-img>
    </v-btn>
    <v-spacer></v-spacer>

    <!-- <v-btn depressed tile text x-large v-if="$vuetify.breakpoint.mdAndUp" class="txt-bold txt-active txt-menu">
        Certificados
    </v-btn> -->
    <v-avatar size="45" color="brown"><span class="white--text headline">GA</span></v-avatar>
    <v-btn icon @click="go_to('user/logout')">
        <v-icon>mdi-export</v-icon>
    </v-btn>
</v-app-bar>