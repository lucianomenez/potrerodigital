<!DOCTYPE html>
<html lang="es">

<head>
    <title>Potrero Digital</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="//cdn.materialdesignicons.com/5.4.55/css/materialdesignicons.min.css">
    <link href="{base_url}dashboard/assets/css/vuetify.min.css" rel="stylesheet">
    <link rel="icon" type="image/png" href="{base_url}files/assets/web/logo.png">
    <link href="{base_url}dashboard/assets/css/main.css" rel="stylesheet">
    <link href="{base_url}dashboard/assets/css/textos.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Inter:wght@300;400;500;600;700;800;900&display=swap"
        rel="stylesheet">
    <script src="{base_url}dashboard/assets/jscript/vue.js"></script>
    <link rel="stylesheet" href="{base_url}dashboard/assets/css/vue-file-agent.css" />
    <script src="{base_url}dashboard/assets/jscript/vue-file-agent.umd.js"></script>
    <script src="https://unpkg.com/axios/dist/axios.min.js"></script>
</head>

<body>