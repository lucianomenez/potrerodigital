<?php
include('_head_vuetify.php')
?>

<div id="app">
    <v-app class="mx-auto overflow-hidden bg_o" height="100vh" v-cloak>
        <top-navbar></top-navbar>
        <left-navbar></left-navbar>
        <main-content></main-content>
    </v-app>
</div>

<!-- Componente navbar -->
<script type="text/x-template" id="top_navbar">
    {top_navbar}
</script>
<!-- Componente navbar -->

<!-- Componente left navbar -->
<script type="text/x-template" id="left_navbar">
    <v-navigation-drawer v-model="drawer" app dark left>
        <v-list-item-group v-model="group" active-class="green--text">
            <v-list-item disabled>
                <v-slide-x-transition>
                    <v-avatar tile height="100%" width="auto" max-width="20%">
                        <v-img contain max-height="100%" d-inline>
                        </v-img>
                    </v-avatar>
                </v-slide-x-transition>
                <v-list-item-content class="white--text font-weight-black text-center" style="letter-spacing:0.2em;">
                  Gabriel Alegre
                </v-list-item-content>
            </v-list-item>
            <v-divider></v-divider>
            <v-list-item disabled>
                <v-list-item-title>Administrador</v-list-item-title>
            </v-list-item>
            <v-divider></v-divider>
            {left_navbar}
        </v-list-item-group>
    </v-navigation-drawer>
</script>
<!-- Componente left navbar -->

<!-- Componente contenido principal -->
<script type="text/x-template" id="main_content">
    <v-main>
        {content}
    </v-main>
</script>
<!-- Componente contenido principal -->

<!-- Files picker -->
<script type="text/x-template" id="file-agent">
    <div class="vfa-demo bg-light pt-3">
    <VueFileAgent
        class="upload-block"
        :ref="ref"
        :multiple="true"
        :deletable="true"
        :meta="true"
        :accept="'.xls,.xlsx'"
        :theme="theme"
        :maxSize="max_size"
        :maxFiles="max_files"
        :editable="false"
        :helpText="label"
        @select="filesSelected($event)"
        @beforedelete="onBeforeDelete($event)"
        @delete="fileDeleted($event)"
    >
    </VueFileAgent>
  </div>
</script>
<!-- Files picker -->

<!-- Variables globales -->
<script>
var main_component;
var top_component;
var left_component;
var file_agent;
</script>

{js}
<?php
include('_scripts_vuetify.php')
?>