<div class="box box-info box-solid {update_class}">
<span class="hidden widget_url">{widget_url}</span>
    <div class="box-header">
        <div class="box-tools pull-right">
            <div class="label bg-aqua">{label}</div>
        </div>
    </div>
    <div class="box-body">
        {content}
    </div><!-- /.box-body -->
</div>
