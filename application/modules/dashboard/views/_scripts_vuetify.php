<script src="https://cdn.jsdelivr.net/npm/vuetify@2.x/dist/vuetify.js"></script>
<script src="{base_url}dashboard/assets/jscript/intersection-observer.js"></script>

<!-- ----------------------------- -->
<script src="{base_url}dashboard/assets/jscript/top_content_vuetify_v1.js"></script>
<script src="{base_url}dashboard/assets/jscript/left_content_vuetify_v1.js"></script>
<script src="{base_url}dashboard/assets/jscript/file_agent_vuetify_v1.js"></script>
<script src="{base_url}dashboard/assets/jscript/app_vuetify_v1.js"></script>
</body>

</html>