<!DOCTYPE html>
<html lang="en">
{head}
<body>
  <section id="container">
    <!-- **********************************************************************************************************************************************************
        TOP BAR CONTENT & NOTIFICATIONS
        *********************************************************************************************************************************************************** -->
    <!--header start-->
    {header}
    <!--header end-->
    <!-- **********************************************************************************************************************************************************
        MAIN SIDEBAR MENU
        *********************************************************************************************************************************************************** -->
    <!--sidebar start-->
    {navbar}
    <!--sidebar end-->
    <!-- **********************************************************************************************************************************************************
        MAIN CONTENT
        *********************************************************************************************************************************************************** -->
    <!--main content start-->

    <section id="main-content">
      <section class="wrapper">
        <!-- page start-->
        <div class="row mt">
          <div class="col-sm-3">
            <section class="panel">
              <div class="panel-body">
                <a href="{base_url}mensajes/nuevo_mensaje" class="btn btn-compose">
                  <i class="fa fa-pencil"></i>  Enviar Mensaje
                  </a>
                <ul class="nav nav-pills nav-stacked mail-nav">
                  <li class="active"><a href="{base_url}mensajes"> <i class="fa fa-inbox"></i> Bandeja de entrada</a></li>
                  <li><a href="{base_url}mensajes/enviados"> <i class="fa fa-envelope-o"></i> Enviados</a></li>
                  <li><a href="{base_url}mensajes/borrados"> <i class="fa fa-trash-o"></i> Borrados</a></li>
                </ul>
              </div>
            </section>
          </div>
          <div class="col-sm-9">
            <section class="panel">
              <header class="panel-heading wht-bg">
                <h4 class="gen-case">
                    Bandeja de Entrada ({count_mensajes})
                    <form action="#" class="pull-right mail-src-position">
                      <div class="input-append">
                        <input type="text" class="form-control " placeholder="Buscar Mensaje">
                      </div>
                    </form>
                  </h4>
              </header>
              <div class="panel-body minimal">
                <div class="mail-option">
                  <div class="chk-all">
                    <div class="pull-left mail-checkbox">
                      <input type="checkbox" class="">
                    </div>
                    <div class="btn-group">
                      <a data-toggle="dropdown" href="#" class="btn mini all">
                        Todos
                        <i class="fa fa-angle-down "></i>
                        </a>
                      <ul class="dropdown-menu">
                        <li><a href="#"> None</a></li>
                        <li><a href="#"> Read</a></li>
                        <li><a href="#"> Unread</a></li>
                      </ul>
                    </div>
                  </div>
                  <div class="btn-group">
                    <a data-original-title="Refresh" data-placement="top" data-toggle="dropdown" href="#" class="btn mini tooltips">
                      <i class=" fa fa-refresh"></i>
                      </a>
                  </div>
                  <div class="btn-group hidden-phone">
                    <a data-toggle="dropdown" href="#" class="btn mini blue">
                      More
                      <i class="fa fa-angle-down "></i>
                      </a>
                    <ul class="dropdown-menu">
                      <li><a href="#"><i class="fa fa-pencil"></i> Mark as Read</a></li>
                      <li><a href="#"><i class="fa fa-ban"></i> Spam</a></li>
                      <li class="divider"></li>
                      <li><a href="#"><i class="fa fa-trash-o"></i> Delete</a></li>
                    </ul>
                  </div>
                  <div class="btn-group">
                    <a data-toggle="dropdown" href="#" class="btn mini blue">
                      Move to
                      <i class="fa fa-angle-down "></i>
                      </a>
                    <ul class="dropdown-menu">
                      <li><a href="#"><i class="fa fa-pencil"></i> Mark as Read</a></li>
                      <li><a href="#"><i class="fa fa-ban"></i> Spam</a></li>
                      <li class="divider"></li>
                      <li><a href="#"><i class="fa fa-trash-o"></i> Delete</a></li>
                    </ul>
                  </div>
                  <ul class="unstyled inbox-pagination">
                    <li><span>1-50 of 99</span></li>
                    <li>
                      <a class="np-btn" href="#"><i class="fa fa-angle-left  pagination-left"></i></a>
                    </li>
                    <li>
                      <a class="np-btn" href="#"><i class="fa fa-angle-right pagination-right"></i></a>
                    </li>
                  </ul>
                </div>
                <div class="table-inbox-wrap ">
                  <table class="table table-inbox table-hover">
                    <tbody>
                      {mensajes}
                      <tr class="{estado}">
                        <td class="inbox-small-cells">
                          <input type="checkbox" class="mail-checkbox">
                        </td>
                        <td class="inbox-small-cells"><i class="fa fa-star"></i></td>
                        <td class="view-message  dont-show"><a href="{base_url}mensajes/ver_mensaje/{id}">{emisor}</a></td>
                        <td class="view-message "><a href="{base_url}mensajes/ver_mensaje/{id}">{asunto}</a></td>
                        <td class="view-message  inbox-small-cells"></td>
                        <td class="view-message  text-right">{fecha}</td>
                      </tr>
                      {/mensajes}
                    </tbody>
                  </table>
                </div>
              </div>
            </section>
          </div>
        </div>
      </section>
      <!-- /wrapper -->
    </section>
    <!--main content end-->
    <!--footer start-->
    <!--footer end-->
  </section>

  <!-- js placed at the end of the document so the pages load faster -->
  <script src="{base_url}elements/assets/lib/jquery/jquery.min.js"></script>
  <script src="{base_url}elements/assets/lib/bootstrap/js/bootstrap.min.js"></script>
  <script class="include" type="text/javascript" src="{base_url}elements/assets/lib/jquery.dcjqaccordion.2.7.js"></script>
  <script src="{base_url}elements/assets/lib/jquery.scrollTo.min.js"></script>
  <script src="{base_url}elements/assets/lib/jquery.nicescroll.js" type="text/javascript"></script>
  <script src="{base_url}elements/assets/lib/jquery.sparkline.js"></script>
  <!--common script for all pages-->
  <script src="{base_url}elements/assets/lib/common-scripts.js"></script>
  <script type="text/javascript" src="{base_url}elements/assets/lib/gritter/js/jquery.gritter.js"></script>
  <script type="text/javascript" src="{base_url}elements/assets/lib/gritter-conf.js"></script>
  <!--script for this page-->
  <script src="{base_url}elements/assets/lib/sparkline-chart.js"></script>
  <script src="{base_url}elements/assets/lib/zabuto_calendar.js"></script>
</body>

</html>
