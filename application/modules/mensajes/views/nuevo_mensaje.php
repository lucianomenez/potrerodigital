<!DOCTYPE html>
<html lang="en">
{head}
<link rel="stylesheet" type="text/css" href="{base_url}mensajes/assets/css/semantic.css">
<link rel="stylesheet" type="text/css" href="{base_url}mensajes/assets/css/semantic.overrides.css">
<body>
  <input type="hidden" id="base_url" name="base_url" value="{base_url}">

  <section id="container">
    <!-- **********************************************************************************************************************************************************
        TOP BAR CONTENT & NOTIFICATIONS
        *********************************************************************************************************************************************************** -->
    <!--header start-->
    {header}
    <!--header end-->
    <!-- **********************************************************************************************************************************************************
        MAIN SIDEBAR MENU
        *********************************************************************************************************************************************************** -->
    <!--sidebar start-->
    {navbar}
    <!--sidebar end-->
    <!-- **********************************************************************************************************************************************************
        MAIN CONTENT
        *********************************************************************************************************************************************************** -->
    <!--main content start-->

    <section id="main-content">
      <section class="wrapper">
        <!-- page start-->
        <div class="row mt">
          <div class="col-sm-3">
            <section class="panel">
              <div class="panel-body">
                <a href="{base_url}mensajes" class="btn btn-compose">
                  <i class="fa fa-inbox"></i>  Bandeja de Entrada
                  </a>
                  <ul class="nav nav-pills nav-stacked mail-nav">
                    <li class="active"><a href="{base_url}mensajes"> <i class="fa fa-inbox"></i> Bandeja de entrada <span class="label label-theme pull-right inbox-notification">{count_mensajes_nuevos}</span></a></li>
                    <li><a href="{base_url}mensajes/enviados"> <i class="fa fa-envelope-o"></i> Enviados</a></li>
                    <li><a href="{base_url}mensajes/borrados"> <i class="fa fa-trash-o"></i> Borrados</a></li>
                  </ul>
              </div>
            </section>
          </div>
          <div class="col-sm-9">
            <section class="panel">
              <header class="panel-heading wht-bg">
                <h4 class="gen-case">
                    Nuevo Mensaje
                  </h4>
              </header>
              <div class="panel-body">
                <div class="compose-mail">
                  <form role="form-horizontal" action="{base_url}mensajes/api/enviar_mensaje" method="post">
                        <h5 class="mt1">Para:<h5>
                        <select name="destinatarios[]" id="destinatarios" class="ui fluid search dropdown" multiple="">
                          {usuarios}
                          <option value="{idu}">{name} {lastname}</option>
                          {/usuarios}
                        </select>
                        <h5 class="mt1">Asunto</h5>
                        <div class="ui input fluid">
                        <input type="text" name="asunto" tabindex="1" id="subject" class="form-control">
                        </div>
                        <h5 class="mt1">Mensaje:</h5>
                        <div class="compose-editor">
                          <textarea  name="mensaje" class="wysihtml5 form-control" rows="10"></textarea>
                        </div>
                    <div class="compose-btn">
                      <button type="submit" class="btn btn-theme btn-md"><i class="fa fa-check"></i> Enviar</button>
                      <button class="btn btn-md">Guardar borrador</button>
                    </div>
                  </form>
                </div>
              </div>
            </section>
          </div>
        </div>
      </section>
      <!-- /wrapper -->
    </section>
    <!--main content end-->
    <!--footer start-->
    <!--footer end-->
  </section>

  <!-- js placed at the end of the document so the pages load faster -->
  <script src="{base_url}elements/assets/lib/jquery/jquery.min.js"></script>
  <script src="{base_url}elements/assets/lib/bootstrap/js/bootstrap.min.js"></script>
  <script class="include" type="text/javascript" src="{base_url}elements/assets/lib/jquery.dcjqaccordion.2.7.js"></script>
  <script src="{base_url}elements/assets/lib/jquery.scrollTo.min.js"></script>
  <script src="{base_url}elements/assets/lib/jquery.nicescroll.js" type="text/javascript"></script>
  <script src="{base_url}elements/assets/lib/jquery.sparkline.js"></script>
  <!--common script for all pages-->
  <script src="{base_url}elements/assets/lib/common-scripts.js"></script>
  <script type="text/javascript" src="{base_url}elements/assets/lib/gritter/js/jquery.gritter.js"></script>
  <script type="text/javascript" src="{base_url}elements/assets/lib/gritter-conf.js"></script>
  <!--script for this page-->
  <script src="{base_url}elements/assets/lib/sparkline-chart.js"></script>
  <script src="{base_url}mensajes/assets/js/semantic.min.js"></script>

  <script src="{base_url}mensajes/assets/js/mensajes2.js"></script>

</body>

</html>
