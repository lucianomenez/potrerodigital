<!DOCTYPE html>
<html lang="en">
{head}
<body>
  <section id="container">
    <!-- **********************************************************************************************************************************************************
        TOP BAR CONTENT & NOTIFICATIONS
        *********************************************************************************************************************************************************** -->
    <!--header start-->
    {header}
    <!--header end-->
    <!-- **********************************************************************************************************************************************************
        MAIN SIDEBAR MENU
        *********************************************************************************************************************************************************** -->
    <!--sidebar start-->
    {navbar}
    <!--sidebar end-->
    <section id="main-content">
      <section class="wrapper">
        <!-- page start-->
        <div class="row mt">
          <div class="col-sm-3">
            <section class="panel">
              <div class="panel-body">
                <a href="{base_url}mensajes/nuevo_mensaje" class="btn btn-compose">
                  <i class="fa fa-pencil"></i>  Enviar Mensaje
                  </a>
                <ul class="nav nav-pills nav-stacked mail-nav">
                  <li class="active"><a href="{base_url}mensajes"> <i class="fa fa-inbox"></i> Bandeja de entrada</a></li>
                  <li><a href="{base_url}mensajes/enviados"> <i class="fa fa-envelope-o"></i> Enviados</a></li>
                  <li><a href="{base_url}mensajes/borrados"> <i class="fa fa-trash-o"></i> Borrados</a></li>
                </ul>
              </div>
            </section>
          </div>
          <div class="col-sm-9">
            <section class="panel">
              <header class="panel-heading wht-bg">
                <h4 class="gen-case">
                    Ver Mensaje
                    <form action="#" class="pull-right mail-src-position">
                      <div class="input-append">
                        <input type="text" class="form-control " placeholder="Search Mail">
                      </div>
                    </form>
                  </h4>
              </header>
              {mensaje}
              <div class="panel-body ">
                <div class="mail-header row">
                  <div class="col-md-8">
                    <h4 class="pull-left">{asunto}</h4>
                  </div>
                </div>
                <div class="mail-sender">
                  <div class="row">
                    <div class="col-md-8">
                      <img src="img/ui-zac.jpg" alt="">
                      <strong>{emisor}</strong>
                       a
                      <strong>Mi</strong>
                    </div>
                    <div class="col-md-4">
                      <p class="date"> {fecha}</p>
                    </div>
                  </div>
                </div>
                <div class="view-mail">
                  <p>{mensaje}</p>
                </div>
                <div class="compose-btn pull-left">
                  <a href="{base_url}mensajes" class="btn btn-sm btn-theme"><i class="fa fa-reply"></i> Responder</a>
                  <button class="btn  btn-sm tooltips" data-original-title="Print" type="button" data-toggle="tooltip" data-placement="top" title=""><i class="fa fa-print"></i> </button>
                  <button class="btn btn-sm tooltips" data-original-title="Trash" data-toggle="tooltip" data-placement="top" title=""><i class="fa fa-trash-o"></i></button>
                </div>
              </div>
              {/mensaje}
            </section>
          </div>
        </div>
      </section>
      <!-- /wrapper -->
    </section>
    <!-- /MAIN CONTENT -->
        <!-- /MAIN CONTENT -->
        <!--main content end-->
    <!--main content end-->
    <!--footer start-->
    <!--footer end-->
  </section>
  <!-- js placed at the end of the document so the pages load faster -->
  <script src="{base_url}elements/assets/lib/jquery/jquery.min.js"></script>
  <script src="{base_url}elements/assets/lib/bootstrap/js/bootstrap.min.js"></script>
  <script class="include" type="text/javascript" src="{base_url}elements/assets/lib/jquery.dcjqaccordion.2.7.js"></script>
  <script src="{base_url}elements/assets/lib/jquery.scrollTo.min.js"></script>
  <script src="{base_url}elements/assets/lib/jquery.nicescroll.js" type="text/javascript"></script>
  <script src="{base_url}elements/assets/lib/jquery.sparkline.js"></script>
  <!--common script for all pages-->
  <script src="{base_url}elements/assets/lib/common-scripts.js"></script>
  <script type="text/javascript" src="{base_url}elements/assets/lib/gritter/js/jquery.gritter.js"></script>
  <script type="text/javascript" src="{base_url}elements/assets/lib/gritter-conf.js"></script>
  <!--script for this page-->
  <script src="{base_url}elements/assets/lib/sparkline-chart.js"></script>
  <script src="{base_url}elements/assets/lib/zabuto_calendar.js"></script>
</body>

</html>
