<?php

/**
 * Funciones para el manejo de datos del container inscripciones
 * @author Luciano Menez <lucianomenez1212@gmail.com>
 * @date 3/05/2016
 *
 */

class Model_mensajes extends CI_Model {

    public function __construct() {
        // Call the Model constructor
        parent::__construct();
        $this->load->library('cimongo/cimongo', '', 'db');
    }

    function get_mensajes($idu){
        $result = array();
        $container = 'potrerodigital.mensajes';
        $query= array('idu_destinatario' => strval($idu));
        $this->db->where($query);
        $result = $this->db->get($container)->result_array();
        return $result;
    }

    function get_mensaje($id){
        $result = array();
        $container = 'potrerodigital.mensajes';
        $query= array('id' => intval($id));
        $this->db->where($query);
        $result = $this->db->get($container)->result_array();
        return $result;
    }

    function update_status($data){
        $result = array();
        $container = 'potrerodigital.mensajes';
        $query= array('id' => $data['id']);
        $this->db->where($query);
        $result = $this->db->update($container, $data);
        return $result;
    }

    function get_users($idu = null){
        $result = array();
        $this->db->select(array("name", "lastname", "idu"));
        $container = 'users';
        $result = $this->db->get($container)->result_array();
        return $result;

    }

    function insertar_mensaje($data){
      $container = 'potrerodigital.mensajes';
      unset($data['destinatarios']);
      $data['id'] = $this->get_last_id();
      $data["id"] = ++$id_last;
      $this->db->insert($container, $data);
      return;
    }

    function get_last_id(){
      $container = 'potrerodigital.mensajes';
      $result = $this->db->get($container)->result_array();
      if (empty($result)){
        $result["id"] = 0;
      }else{
        $result = end($result);
      }
      return $result['id'];
    }



}
