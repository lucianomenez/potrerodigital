<?php

class Mensajes extends MX_Controller {

  function __construct() {
      parent::__construct();
      $this->base_url = base_url();
      $this->module_url = base_url() . $this->router->fetch_module() . '/';
      $this->user->authorize();
      $this->load->module('elements');
      $this->load->helper('file');
      $this->load->helper('url');
      $this->load->library('parser');
      $this->load->model('user/user');
      $this->load->model('Model_mensajes');
      $this->idu = $this->user->idu;
      $this->alumno = $this->user->get_user($this->idu);

  }

    //==== MAIN ALUMNO

    function Index() {
      $data["base_url"] = $this->base_url;
      $data["module_url"] = $this->module_url;
      $data["idu"] = $this->idu;
      $data["head"] = $this->elements->head();
      $data["header"] = $this->elements->ui_header();
      $data["navbar"] = $this->elements->ui_navbar();
      $data["mensajes"] = $this->Model_mensajes->get_mensajes($this->idu);
      $data["count_mensajes"] = count($data['mensajes']);

      return $this->parser->parse("mensajes", $data, false, false);
     	//$customData['usercan_create']=true;
    }

    function ver_mensaje($id) {
      $data["base_url"] = $this->base_url;
      $data["module_url"] = $this->module_url;
      $data["idu"] = $this->idu;
      $data["head"] = $this->elements->head();
      $data["header"] = $this->elements->ui_header();
      $data["navbar"] = $this->elements->ui_navbar();
      $data['mensaje'] = $this->Model_mensajes->get_mensaje($id);
      if($data["mensaje"][0]['estado'] == "unread"){
        $data["mensaje"][0]['estado'] = "readed";
        $this->Model_mensajes->update_status($data['mensaje'][0]);
      }
      return $this->parser->parse("ver_mensaje", $data, false, false);
     	//$customData['usercan_create']=true;
    }

    function nuevo_mensaje() {
      $data["base_url"] = $this->base_url;
      $data["module_url"] = $this->module_url;
      $data["idu"] = $this->idu;
      $data["head"] = $this->elements->head();
      $data["header"] = $this->elements->ui_header();
      $data["navbar"] = $this->elements->ui_navbar();
      $data["usuarios"] = $this->Model_mensajes->get_users();
      return $this->parser->parse("nuevo_mensaje", $data, false, false);
     	//$customData['usercan_create']=true;
    }







} //

?>
