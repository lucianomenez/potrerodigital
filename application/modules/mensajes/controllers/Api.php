<?php

class Api extends MX_Controller {

  function __construct() {
      parent::__construct();
      $this->base_url = base_url();
      $this->module_url = base_url() . $this->router->fetch_module() . '/';
      $this->user->authorize();
      $this->load->module('elements');
      $this->load->helper('file');
      $this->load->helper('url');
      $this->load->library('parser');
      $this->load->model('app');
      $this->load->model('user/user');
      $this->load->model('Model_mensajes');
      $this->idu = $this->user->idu;
      $this->alumno = $this->user->get_user($this->idu);

  }

    //==== MAIN ALUMNO

    function get_nombres() {
      $data = array();
      $nombres = $this->Model_mensajes->get_nombres();
      foreach ($nombres as $nombre){
        $data[] = $nombre['name'];
        $data[] = $nombre["lastname"];
      }
      echo json_encode($data);
     	//$customData['usercan_create']=true;
    }

    function enviar_mensaje() {
      $data = $this->input->post();
      $data['estado'] = "unread";
      $data['emisor'] = $this->alumno->name." ".$this->alumno->lastname;
      $data['idu_emisor'] = $this->idu;
      $data['fecha'] = date("d-m-Y");
      foreach ($data['destinatarios'] as $destinatario){
        $data['idu_destinatario'] = $destinatario;
        $this->Model_mensajes->insertar_mensaje($data);
      }
     	redirect (base_url().'mensajes');
    }







} //

?>
