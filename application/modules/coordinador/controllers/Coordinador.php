<?php

class Coordinador extends MX_Controller {

  function __construct() {
      parent::__construct();
      $this->base_url = base_url();
      $this->module_url = base_url() . $this->router->fetch_module() . '/';
      $this->user->authorize();
      $this->load->module('elements');
      $this->load->helper('file');
      $this->load->helper('url');
      $this->load->library('parser');
      $this->load->model('app');
      $this->load->model('alumno/Model_alumno');
      $this->load->model('Model_coordinador');
      $this->load->model('user/user');
      $this->idu = $this->user->idu;
      $this->alumno = $this->user->get_user($this->idu);

  }

    //==== MAIN ALUMNO

    function Index() {
      $data["base_url"] = $this->base_url;
      $data["module_url"] = $this->module_url;
      $data["idu"] = $this->idu;
      $data["head"] = $this->elements->head();
      $data["header"] = $this->elements->ui_header();
      $data["navbar"] = $this->elements->ui_navbar_coordinador();
      $data["inscripciones"] = $this->Model_alumno->get_inscripciones();
      $data["usuarios_temporales"] = $this->Model_coordinador->get_usuarios_temporales();
      foreach ($data["usuarios_temporales"] as &$usuario){
        switch ($usuario['estado']) {
            case "pendiente":
                $usuario['class'] = "warning";
                break;
            case "aprobado":
                $usuario['class'] = "success";
                break;
            case "rechazado":
                $usuario['class'] = "danger";
                break;
        }

      }


      echo $this->parser->parse("index", $data, true, true);
     	//$customData['usercan_create']=true;
    }

    function cursos($id) {
      $data["base_url"] = $this->base_url;
      $data["module_url"] = $this->module_url;
      $data["idu"] = $this->idu;
      $segments = $this->uri->segment_array();

      $comision = "Comisión ".$segments[3];
      $data['users'] = $this->Model_coordinador->get_usuarios_por_comision($comision);
      $data["header"]= $this->elements->ui_header();
      $data["navbar"]= $this->elements->ui_navbar_coordinador();
      $data["head"]= $this->elements->head();


      echo $this->parser->parse("cursos", $data, true, true);
     	//$customData['usercan_create']=true;
    }

    function carreras() {
      $data["base_url"] = $this->base_url;
      $data["module_url"] = $this->module_url;
      $data["idu"] = $this->idu;
      $data["header"]= $this->elements->ui_header();
      $data["navbar"]= $this->elements->ui_navbar();
      $data["head"]= $data->elements->ui_head();
      echo $this->parser->parse("carreras", $data, true, true);
     	//$customData['usercan_create']=true;
    }


    function tareas() {
      $data["base_url"] = $this->base_url;
      $data["module_url"] = $this->module_url;
      $data["idu"] = $this->idu;
      $data["header"]= $this->elements->ui_header();
      $data["navbar"]= $this->elements->ui_navbar();
      $data["head"]= $data->elements->ui_head();
      echo $this->parser->parse("tareas", $data, true, true);
     	//$customData['usercan_create']=true;
    }






} //

?>
