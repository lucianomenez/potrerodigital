<?php

class Api extends MX_Controller {

  function __construct() {
      parent::__construct();
      $this->base_url = base_url();
      $this->module_url = base_url() . $this->router->fetch_module() . '/';
      $this->user->authorize();
      $this->load->module('elements');
      $this->load->helper('file');
      $this->load->helper('url');
      $this->load->library('parser');
      $this->load->model('app');
      $this->load->model('user/user');
      $this->load->model('Model_coordinador');
      $this->load->model('alumno/Model_alumno');

      $this->idu = $this->user->idu;
      $this->alumno = $this->user->get_user($this->idu);

  }

    //==== MAIN ALUMNO

    function cargar_clase() {
      $data = $this->input->post();
      $data["fecha"] = date("d-m-Y");
      $data["nombre"] = $this->alumno->name." ".$this->alumno->lastname;
      $curso = $this->Model_alumno->get_cursos($data["id"]);
      $curso[0]['clases'][] = $data;
      $this->Model_alumno->update_curso($curso[0]);
      redirect (base_url()."docente");

     	//$customData['usercan_create']=true;
    }

    function aprobar_usuario() {
      $data = $this->input->post();
      $data["estado"] = "aprobado";
      $this->Model_coordinador->update_usuarios_temporales($data);
      return;
    }

    function rechazar_usuario() {
      $data = $this->input->post();
      $data["estado"] = "rechazado";
      $this->Model_coordinador->update_usuarios_temporales($data);
      return;
    }




} //

?>
