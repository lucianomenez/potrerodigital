<?php

/**
 * Funciones para el manejo de datos del container inscripciones
 * @author Luciano Menez <lucianomenez1212@gmail.com>
 * @date 3/05/2016
 *
 */

class Model_coordinador extends CI_Model {

    public function __construct() {
        // Call the Model constructor
        parent::__construct();
        $this->load->library('cimongo/cimongo', '', 'db');
    }

    function get_usuarios_temporales($id = null){
        $result = array();
        $container = 'users_temporal';
        $query=array();
        $result = $this->db->get($container)->result_array();
        return $result;
    }

    function update_usuarios_temporales($data){
        $result = array();
        $container = 'users_temporal';
        $query= array('nick' => $data['nick']);
        $this->db->where($query);
        $result = $this->db->update($container, $data);
        return $result;
    }

    function get_usuarios_por_comision($comision){
        $result = array();
        $container = 'users';
        $query= array('curso' => $comision);
        $this->db->where($query);
        $result = $this->db->get($container)->result_array();
        return $result;
    }


}
