<!DOCTYPE html>
<html lang="en">
{head}
<link rel="stylesheet" href="{base_url}elements/assets/css/to-do.css">

<body>
  <section id="container">
    <input type="hidden" id="base_url" name="base_url" value="{base_url}">

    <!-- **********************************************************************************************************************************************************
        TOP BAR CONTENT & NOTIFICATIONS
        *********************************************************************************************************************************************************** -->
    <!--header start-->
    {header}
    <!--header end-->
    <!-- **********************************************************************************************************************************************************
        MAIN SIDEBAR MENU
        *********************************************************************************************************************************************************** -->
    <!--sidebar start-->
    {navbar}
    <!--sidebar end-->
  <section id="main-content">
  <section class="wrapper">
    <div class="row mt">
      <div class="col-md-12">

      <div class="content-panel">
          <table class="table table-striped table-advance table-hover">
            <h4><i class="fa fa-angle-right"></i> Listado de Inscriptos</h4>
            <hr>
            <thead>
              <tr>
                <th><i class="fa fa-name"></i> Nombre y Apellido</th>
                <th class="hidden-phone"><i class="fa fa-carrer"></i> Residencia</th>
                <th><i class="fa fa-email"></i> Email</th>
                <th><i class=" fa fa-status"></i> Telefono</th>
                <th></th>
              </tr>
            </thead>
            <tbody>
              {users}
              <tr>
                <td>
                  <a href="{base_url}perfil/ver_perfil/{idu}">{name} {lastname}</a>
                </td>
                <td class="hidden-phone">{provincia}, {city}</td>
                <td>{email}</td>
                <td><span class="label label-info label-mini">{cel_phone}</span></td>
                <td>
                  <button class="btn btn-success btn-xs"><i class="fa fa-check"></i></button>
                  <button class="btn btn-danger btn-xs"><i class="fa fa-trash-o "></i></button>
                </td>
              </tr>
              {/users}
            </tbody>
          </table>
        </div>
      <!-- /content-panel -->
      <!-- /col-md-12 -->
      <div class="content-panel">
        <table class="table table-striped table-advance table-hover">
          <h4><i class="fa fa-angle-right"></i> Listado de Solicitudes de Usuarios</h4>
          <hr>
          <thead>
            <tr>
              <th><i class="fa fa-name"></i> Nombre y Apellido</th>
              <th class="hidden-phone"><i class="fa fa-carrer"></i> Teléfono</th>
              <th><i class="fa fa-email"></i> Email</th>
              <th><i class="fa fa-nick"></i> DNI</th>
              <th><i class=" fa fa-status"></i> Estado</th>
              <th></th>
            </tr>
          </thead>
          <tbody>
            {usuarios_temporales}
            <tr>
              <td>
                <a href="{base_url}perfil/ver_perfil/{idu}">{name} {lastname}</a>
              </td>
              <td class="hidden-phone">{phone}</td>
              <td>{email}</td>
              <td>{nick}</td>
              <td><span class="label label-{class} label-mini">{estado}</span></td>
              <td>
                <button data-nick="{nick}" class="btn btn-success btn-xs aprobar_usuario"><i class="fa fa-check"></i></button>
                <button data-nick="{nick}" class="btn btn-danger btn-xs rechazar_usuario"><i class="fa fa-trash-o "></i></button>
              </td>
            </tr>
            {/usuarios_temporales}
          </tbody>
        </table>
      </div>

      </div>
    </div>
    <!-- /row -->
   </section>
  </section>
    <!--footer end-->
  </section>

  <!-- js placed at the end of the document so the pages load faster -->
  <script src="{base_url}elements/assets/lib/jquery/jquery.min.js"></script>
  <script src="{base_url}elements/assets/lib/bootstrap/js/bootstrap.min.js"></script>
  <script class="include" type="text/javascript" src="{base_url}elements/assets/lib/jquery.dcjqaccordion.2.7.js"></script>
  <script src="{base_url}elements/assets/lib/jquery.scrollTo.min.js"></script>
  <script src="{base_url}elements/assets/lib/jquery.nicescroll.js" type="text/javascript"></script>
  <script src="{base_url}elements/assets/lib/jquery.sparkline.js"></script>
  <!--common script for all pages-->
  <script src="{base_url}elements/assets/lib/common-scripts.js"></script>
  <script type="text/javascript" src="{base_url}elements/assets/lib/gritter/js/jquery.gritter.js"></script>
  <script type="text/javascript" src="{base_url}elements/assets/lib/gritter-conf.js"></script>
  <!--script for this page-->
  <script src="{base_url}elements/assets/lib/sparkline-chart.js"></script>
  <script src="{base_url}elements/assets/lib/zabuto_calendar.js"></script>
  <script src="{base_url}coordinador/assets/js/coordinador.js"></script>

</body>

</html>
