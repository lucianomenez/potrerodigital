var base_url = $("#base_url").val();

$('.rechazar_usuario').on('click', function(e){
    var nick = $(this).attr("data-nick");
    $.ajax({
     url : base_url + 'coordinador/api/rechazar_usuario',
     type: 'POST',
     data: {
       nick : nick
     },
     success: function(data){
       location.reload();
     }
    });

});

$('.aprobar_usuario').on('click', function(e){
    var nick = $(this).attr("data-nick");
    $.ajax({
     url : base_url + 'coordinador/api/aprobar_usuario',
     type: 'POST',
     data: {
       nick : nick
     },
     success: function(data){
       location.reload();
     }
    });

});
