<!DOCTYPE HTML PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xml:lang="en" xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
    <link href="https://fonts.googleapis.com/css2?family=Inter:wght@400;500;700&display=swap" rel="stylesheet">
    <link href="{base_url}certificados/assets/css/style.css" rel="stylesheet">
    <title>Certificado</title>

<body style="background-image: url({base_url}certificados/assets/css/img/back.png);">

    <section class="contenedor" style="">
        <img class="logo" src="{base_url}certificados/assets/css/img/logo.png">
        <div class="title">Certificado de Capacitación</div>
        <div class="p1">Por cuanto <b>{name} {lastname}</b></div>
        <div class="p2">Ha aprobado el Trayecto de Formación: <b>“{real_name}”</b>, cursado durante un cuatrimestre bajo
            la modalidad virtual, se extiende el presente
            certificado en el mes de <b>Diciembre de 2020.</b></div>

        <table class="grid-firmas">
            <tr>
                <td class="section-firmas">
                    <div class="item">
                        <img class="firma" src="{base_url}certificados/assets/css/img/firma1.png">
                        <div class="nombre">Juan José Bertamoni</div>
                        <div class="cargo">Director Potrero Digital</div>
                    </div>
                </td>
                <td class="section-firmas">
                    <div class="item">
                        <img class="firma" src="{base_url}certificados/assets/css/img/{docente_slug}.png">
                        <div class="nombre">{Docente}</div>
                        <div class="cargo">Docente</div>
                    </div>
                </td>
                {firma2}
            </tr>
        </table>

    </section>

    <table class="grid-footer">
        <tr>
            {footer}
        </tr>
    </table>

</body>

</html>