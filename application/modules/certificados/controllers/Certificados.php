<?php
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

include "application/composer_vendor/autoload.php";

use Dompdf\Dompdf;

/*
 * Paneles de Navegación
 *
 * @author Héctor Gabriel Alegre <gabriel.h.alegre@gmail.com>
 */

class Certificados extends MX_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->base_url = base_url();
        $this->module_url = base_url() . $this->router->fetch_module() . '/';
        $this->user->authorize();
        $this->load->library('parser');
        $this->load->helper('url');
        $this->load->model('app');
        $this->load->model('user/user');
        $this->load->model('Model_certificados');
        $this->load->config('config');
        $this->idu = $this->user->idu;
    }

    public function index()
    {
        $data['base_url'] = $this->base_url;
        $data['module_url'] = $this->module_url;
        Modules::run('dashboard/dashboard', 'certificados/json/certificados.json', $debug, $data);
    }

    public function get_alumnos_by_group()
    {
        $alumnos = $this->Model_certificados->get_alumnos_by_group();
        echo json_encode($alumnos);
    }

    //Genera pdf a partir de un dni
    public function generate_pdf($idnumber)
    {
        $alumnos = $this->Model_certificados->get_alumnos(null, $idnumber);

        if ($alumnos) {
            if (is_array($alumnos)) {
                foreach ($alumnos as $key => $alumno) {
                    $this->guardar_pdf($alumno);
                }
            }
        }
    }

    //Busca pdf a partir de un dni, si no lo encuentra lo genera
    public function get_pdf($idnumber)
    {
        $certifiados_list = [];
        $alumnos = $this->Model_certificados->get_alumnos(null, $idnumber);

        if ($alumnos) {
            if (is_array($alumnos)) {
                foreach ($alumnos as $key => $alumno) {
                    $lugar = $alumno['lugar'];
                    $cohorte = $alumno['cohorte'];
                    $ano = $alumno['ano'];
                    $curso = $this->slugify($alumno['curso']);

                    $ruta = "files/assets/certificados/" . $lugar . "/" . $ano . "/" . $cohorte . "/" . $curso . "/";
                    $certificado = $this->base_url . $ruta . $lugar . "-" . $cohorte . "-" . $curso . $ano . "-" . $alumno['idnumber'] . ".pdf";

                    array_push($certifiados_list, $certificado);
                }
            }
        }
        return json_encode($certifiados_list);
    }

    public function get_pdf_all()
    {
        $alumnos = $this->Model_certificados->get_alumnos(false);

        if (is_array($alumnos)) {
            foreach ($alumnos as $key => $alumno) {

                $this->guardar_pdf($alumno);
            }
        }

        echo json_decode('success');
    }

    //Lista todos los pdfs generados
    private function listar_certificados($folder)
    {
        $lugar = $folder['lugar'];
        $cohorte = $folder['cohorte'];
        $ano = $folder['ano'];
        $curso = $this->slugify($alumno['curso']);
        $return = $this->listFiles(FCPATH . 'application/modules/files/assets/certificados/' . $lugar . '/' . $ano . '/' . $cohorte . '/' . $curso);
        return $return;
    }

    //Guarda un pdf
    private function guardar_pdf($alumno)
    {
        $lugar = $alumno['lugar'];
        $cohorte = $alumno['cohorte'];
        $ano = $alumno['ano'];
        $curso = $this->slugify($alumno['curso']);

        // Make dir
        if (!file_exists(FCPATH . 'application/modules/files/assets/certificados/' . $lugar . '/' . $ano . '/' . $cohorte . '/' . $curso)) {
            @mkdir(FCPATH . 'application/modules/files/assets/certificados/' . $lugar . '/' . $ano . '/' . $cohorte . '/' . $curso . '/', 0777, true);
        }

        $dompdf = new Dompdf();
        $alumno['docente_slug'] = $this->slugify($alumno['Docente']);
        $alumno['docente_slug2'] = $this->slugify($alumno['Docente2']);
        $dompdf->loadHtml($this->get_certificado($alumno));
        $dompdf->set_option('isRemoteEnabled', true);
        $dompdf->setPaper('A4', 'landscape');
        $dompdf->getOptions()->setDefaultFont('Inter');
        $dompdf->set_option('enable_php', true);
        //Donde guardar el documento
        $rutaGuardado = FCPATH . "application/modules/files/assets/certificados/" . $lugar . "/" . $ano . "/" . $cohorte . "/" . $curso . '/';
        //Nombre del Documento.
        $nombreArchivo = $lugar . "-" . $cohorte . "-" . $curso . $ano . "-" . $alumno['idnumber'] . ".pdf";

        //Renderiza el archivo primero
        $dompdf->render();

        //Guardalo en una variable
        $output = $dompdf->output();

        file_put_contents($rutaGuardado . $nombreArchivo, $output);
        $ruta = "files/assets/certificados/" . $lugar . "/" . $ano . "/" . $cohorte . "/" . $curso . "/";
        return $this->base_url . $ruta . $nombreArchivo;
    }

    //Devuelve el template para generar pdf
    private function get_certificado($alumno)
    {
        $data = $alumno;

        $data['footer'] = $this->get_footer($alumno['origen'] . trim($alumno['cohorte']) . '-' . $this->slugify($alumno['curso']) . '-' . $alumno['ano']);
        $data['base_url'] = $this->base_url;
        $data['module_url'] = $this->module_url;
        $data['real_name'] = trim($data['real_name']);
        $data['firma2'] = false;
        if ($alumno['Docente2'] && $alumno['Docente2'] != '') {
            $data['firma2'] = '
            <td class="section-firmas">
            <div class="item">
                <img class="firma" src="' . $this->base_url . 'certificados/assets/css/img/' . $data['docente_slug2'] . '.png">
                <div class="nombre">' . $data['Docente2'] . '</div>
                <div class="cargo">Docente</div>
            </div>
            </td>';
        }
        $template = $this->parser->parse('certificados_pdf', $data, true, true);
        return $template;
    }

    //Carga el footer correspondiente para generar pdf
    private function get_footer($file)
    {
        $data['base_url'] = $this->base_url;
        $data['module_url'] = $this->module_url;
        $template = $this->parser->parse('footer_' . $file, $data, true, true);
        return $template;
    }

    private function listFiles($directorio)
    {
        $return = [];
        if (is_dir($directorio)) {
            if ($dir = opendir($directorio)) {

                while (($archivo = readdir($dir)) !== false) {
                    if ($archivo != '.' && $archivo != '..') {
                        $return[] = $archivo;
                    }
                }
                closedir($dir);
                return $return;
            }
        } else {
            echo 'No Existe el directorio';
        }
    }

    public function count_alumnos_unicos($curso = null)
    {
        $alumnos = $this->Model_certificados->count_alumnos_unicos($curso);
        var_dump($alumnos);
    }
    public function enviar_certificados()
    {

        $alumnos = $this->Model_certificados->get_alumnos(true, false);

        $alumnos_email = [];
        if (is_array($alumnos)) {
            foreach ($alumnos as $key => $alumno) {
                $lugar = $alumno['lugar'];
                $cohorte = $alumno['cohorte'];
                $ano = $alumno['ano'];
                $curso = $this->slugify($alumno['curso']);
                $ruta = "files/assets/certificados/" . $lugar . "/" . $ano . "/" . $cohorte . "/" . $curso . "/";
                $certificado = $lugar . "-" . $cohorte . "-" . $curso . $ano . "-" . $alumno['idnumber'] . ".pdf";
                $msg['c_email'] = "potrerodigital@compromiso.org";
                $msg['c_name'] = 'Potrero';
                $msg['c_lastname'] = 'Digital';
                $msg['c_subject'] = 'Certificado ' . $alumno['curso'];
                $data_email['ruta'] = "http://plataforma.potrerodigital.org/" . $ruta . $certificado;
                $data_email['curso'] = $alumno['real_name'];
                $data_email['name'] = $alumno['name'] . ' ' . $alumno['lastname'];
                $data_email['logos'] = [];

                switch ($alumno['curso']) {

                        // case 'Emprendedorismo':
                        //     $data_email['logos'][0]['logo'] = "<img height=45px src=http://plataforma.potrerodigital.org/certificados/assets/css/img/jpmorgan.png>";
                        //     $data_email['logos'][1]['logo'] = "<img height=50px src=http://plataforma.potrerodigital.org/certificados/assets/css/img/compromiso.png>";
                        //     break;
                        // case 'Publicidad en Google':
                        //     $data_email['logos'][0]['logo'] = "<img height=50px src=http://plataforma.potrerodigital.org/certificados/assets/css/img/google.png>";
                        //     $data_email['logos'][1]['logo'] = "<img height=50px src=http://plataforma.potrerodigital.org/certificados/assets/css/img/compromiso.png>";
                        //     break;
                        // case 'Programación Back End':
                        //     $data_email['logos'][0]['logo'] = "<img height=50px src=http://plataforma.potrerodigital.org/certificados/assets/css/img/logo-DH.png>";
                        //     $data_email['logos'][1]['logo'] = "<img height=50px src=http://plataforma.potrerodigital.org/certificados/assets/css/img/mecenazgo.png>";
                        //     $data_email['logos'][2]['logo'] = "<img height=50px src=http://plataforma.potrerodigital.org/certificados/assets/css/img/compromiso.png>";
                        //     break;
                        // case 'Educación Financiera':
                        //     $data_email['logos'][0]['logo'] = "<img height=45px src=http://plataforma.potrerodigital.org/certificados/assets/css/img/jpmorgan.png>";
                        //     $data_email['logos'][1]['logo'] = "<img height=50px src=http://plataforma.potrerodigital.org/certificados/assets/css/img/compromiso.png>";
                        //     break;
                        // case 'Programación en Python':
                        //     $data_email['logos'][0]['logo'] = "<img height=50px src=http://plataforma.potrerodigital.org/certificados/assets/css/img/logo-DH.png>";
                        //     $data_email['logos'][1]['logo'] = "<img height=50px src=http://plataforma.potrerodigital.org/certificados/assets/css/img/mecenazgo.png>";
                        //     $data_email['logos'][2]['logo'] = "<img height=50px src=http://plataforma.potrerodigital.org/certificados/assets/css/img/compromiso.png>";
                        //     break;

                        // case 'Habilidades Socioemocionales':
                        //     $data_email['logos'][0]['logo'] = "<img height=50px src=http://plataforma.potrerodigital.org/certificados/assets/css/img/edlp.jpg>";
                        //     $data_email['logos'][1]['logo'] = "<img height=25px src=http://plataforma.potrerodigital.org/certificados/assets/css/img/innova.png>";
                        //     $data_email['logos'][2]['logo'] =  "<img height=50px src=http://plataforma.potrerodigital.org/certificados/assets/css/img/itau.png>";
                        //     $data_email['logos'][3]['logo'] =  "<img height=50px src=http://plataforma.potrerodigital.org/certificados/assets/css/img/compromiso.png>";
                        //     break;
                        // case 'Programación Front End':
                        //     $data_email['logos'][0]['logo'] = "<img height=50px src=http://plataforma.potrerodigital.org/certificados/assets/css/img/edlp.png>";
                        //     $data_email['logos'][1]['logo'] =  "<img height=25px src=http://plataforma.potrerodigital.org/certificados/assets/css/img/innova.png>";
                        //     $data_email['logos'][2]['logo'] = "<img height=50px src=http://plataforma.potrerodigital.org/certificados/assets/css/img/itau.png>";
                        //     $data_email['logos'][3]['logo'] =  "<img height=50px src=http://plataforma.potrerodigital.org/certificados/assets/css/img/logo-DH.png>";
                        //     $data_email['logos'][4]['logo'] =  "<img height=50px src=http://plataforma.potrerodigital.org/certificados/assets/css/img/compromiso.png>";
                        //     break;
                        // case 'Community Manager':
                        //     $data_email['logos'][0]['logo'] = "<img height=50px src=http://plataforma.potrerodigital.org/certificados/assets/css/img/edlp.jpg>";
                        //     $data_email['logos'][1]['logo'] =  "<img height=25px src=http://plataforma.potrerodigital.org/certificados/assets/css/img/innova.png>";
                        //     $data_email['logos'][2]['logo'] = "<img height=50px src=http://plataforma.potrerodigital.org/certificados/assets/css/img/itau.png>";
                        //     $data_email['logos'][3]['logo'] =  "<img height=50px src=http://plataforma.potrerodigital.org/certificados/assets/css/img/google.png>";
                        //     $data_email['logos'][4]['logo'] =  "<img height=50px src=http://plataforma.potrerodigital.org/certificados/assets/css/img/compromiso.png>";
                        //     break;
                }
                $msg['c_message'] = "<html><head><style>img{margin-right:50px;}</style></head><body>" . $this->parser->parse('certificados/email', $data_email, true, true) . "</body></html>";

                $this->send_mail($msg, trim(strtolower($alumno['email'])), false);

                echo $key . ' -> ' . trim(strtolower($alumno['email'])) . '<br>';

                $alumnos_email[] = $alumno['email'];
            }
        }
        echo 'Finalizado';
    }

    private function send_mail($msg = null, $to = "gabriel.h.alegre@gmail.com", $redirect = true)
    {
        if (!$msg) {
            $msg = $this->input->post();
        }

        $data = '{
                   "from": { "email": "' . $msg['c_email'] . '" , "name" : "' . $msg['c_name'] . ' ' . $msg['c_lastname'] . '" },
                   "reply_to": {
                 		"email": "' . $msg['c_email'] . '",
                 		"name": "' . $msg['c_name'] . ' ' . $msg['c_lastname'] . '"
                 	},
                   "subject": "' . $msg['c_subject'] . '",
                   "content": {"html":"' . $msg['c_message'] . '"},
                   "recipients": [{"to": {"email": "' . $to . '"}}]
               }';

        $ch = curl_init('https://transactional.myperfit.com/v1/mail/send');
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            "Authorization: Bearer efecinco-tr-JKbzGIQgidLFlFhcIGcAeRIpKdOahbgP",
            "Content-Type: application/json",
        ));
        $result = curl_exec($ch);
        $cpData['base_url'] = $this->base_url;
        if ($redirect) {
            redirect($this->base_url, 'refresh');
        } else {
            return 'success';
        }
    }

    public function get_emails()
    {

        $ch = curl_init('https://api.myperfit.com/v2/cora/activity');
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            "Authentication: Bearer efecinco-tr-JKbzGIQgidLFlFhcIGcAeRIpKdOahbgP",
            "Content-Type: application/json",
        ));
        $result = curl_exec($ch);
        var_dump($result);
        exit;
    }

    private function slugify($text)
    {

        $table = array(
            "à" => 'a', "ä" => 'a', "á" => 'a', "â" => 'a', "æ" => 'a', "å" => 'a', "ë" => 'e', "è" => 'e', "é" => 'e', "ê" => 'e', "î" => 'i', "ï" => 'i', "ì" => 'i', "í" => 'i', "ò" => 'o', "ó" => 'o', "ö" => 'o', "ô" => 'o', "ø" => 'o', "ù" => 'o', "ú" => 'u', "ü" => 'u', "û" => 'u', "ñ" => 'n', "ç" => 'c', "ß" => 's', "ÿ" => 'y', "œ" => 'o', "ŕ" => 'r', "ś" => 's', "ń" => 'n', "ṕ" => 'p', "ẃ" => 'w', "ǵ" => 'g', "ǹ" => 'n', "ḿ" => 'm', "ǘ" => 'u', "ẍ" => 'x', "ź" => 'z', "ḧ" => 'h', "·" => '-', "/" => '-', "_" => '-', "," => '-', "=>" => '-', ";" => '-', " " => '-', "&" => '-and-', "." => '', "(" => '', ")" => '',
        );

        // lowercase
        $text = strtolower($text);
        // Trim - from start and end of text
        $text = trim($text);
        // Replace special characters using the hash map
        $text = strtr($text, $table);

        if (empty($text)) {
            return 'n-a';
        }

        return $text;
    }
}