<?php

/**
 * Funciones para el manejo de datos del container inscripciones
 * @author Luciano Menez <lucianomenez1212@gmail.com>
 * @date 3/05/2016
 *
 */

class Model_certificados extends CI_Model
{

    public function __construct()
    {
        // Call the Model constructor
        parent::__construct();
        $this->load->library('cimongo/cimongo', '', 'db');
    }

    public function get_alumnos($pdf_generado=null,$enviado=null,$idnumber = null, $origen = null, $limit = 9999, $skip = 0)
    {
        $query = ['status' => 'activo'];
        if ($origen!==null) {
            $query['origen'] = $origen;
        }
        if ($pdf_generado!==null) {
            $query['pdf_generado'] = $pdf_generado;
        }

        if ($enviado!==null) {
            $query['email_enviado'] = $enviado;
        }

        if ($idnumber!==null) {
            $query['idnumber'] = $idnumber;
        }
        $queryArray =
            [
            [
                '$match' => $query,
            ],
            [
                '$project' => [
                    '_id' => 0,
                ],
            ],
            [
                '$sort' => ['lastname' => 1],
            ],
            [
                '$limit' => $skip + $limit,
            ],
            [
                '$skip' => $skip,
            ],

        ];

        $rs = $this->mongowrapper->db->selectCollection('container.certificados')->aggregateCursor($queryArray);
        $result = iterator_to_array($rs, false);

        return $result;
    }
    public function count_alumnos_unicos( $curso = null, $limit = 9999, $skip = 0)
    {
        $curso=str_replace("%20", " ", $curso);
        $curso=str_replace("%20", " ", $curso);
        $curso=str_replace("%C3%B3", "ó", $curso);
  
        $query = ['status' => 'activo'];
        if ($curso) {
            $query['curso'] = $curso;
        }

        $queryArray =
            [
            [
                '$match' => $query,
            ],
            [
                '$group' => [
                    '_id' => '$idnumber',
                    'firstIdnumber' =>[ '$first'=>'$idnumber']
                ],
            ],
            [
                '$sort' => ['firstIdnumber' => 1],
            ],
            [
                '$limit' => $skip + $limit,
            ],
            [
                '$skip' => $skip,
            ],

        ];

        $rs = $this->mongowrapper->db->selectCollection('container.certificados')->aggregateCursor($queryArray);
        $result = iterator_to_array($rs, false);

        return $result;
    }

    public function get_alumnos_by_group($idnumber = null, $origen = null, $limit = 9999, $skip = 0)
    {
        $query = ['status' => 'activo'];
        if ($origen) {
            $query['origen'] = $origen;
        }

        if ($idnumber) {
            $query['idnumber'] = $idnumber;
        }
        $queryArray =
            [
            [
                '$match' => $query,
            ],
            [
                '$project' => [
                    '_id' => 0,
                ],
            ],
            [
                '$group' => [
                    '_id' => ['$lugar', '$ano', '$cohorte'],
                    'alumnos' => [
                        '$addToSet' => [
                            'idnumber' => '$idnumber',
                            'lastname' => '$lastname',
                            'lugar' => '$lugar',
                            'ano' => '$ano',
                            'cohorte' => '$cohorte',
                            'origen' => '$origen',
                        ],
                    ],
                ],
            ],
            [
                '$addFields' => [
                  'origen' => ['$first'=>'$alumnos.origen'],
                  'ano' => ['$first'=>'$alumnos.ano'],
                  'cohorte' => ['$first'=>'$alumnos.cohorte'],
                  'lugar' => ['$first'=>'$alumnos.lugar'],
                ],
            ],
            [
                '$sort' => ['alumnos.lastname' => 1],
            ],
            [
                '$limit' => $skip + $limit,
            ],
            [
                '$skip' => $skip,
            ],

        ];

        $rs = $this->mongowrapper->db->selectCollection('container.certificados')->aggregateCursor($queryArray);
        $result = iterator_to_array($rs, false);

        return $result;
    }

}
