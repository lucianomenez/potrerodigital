<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="Dashboard">
  <meta name="keyword" content="Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">
  <title>Potrero Digital - Campus Virtual</title>

  <!-- Favicons -->
  <link href="http://potrerodigital.org/img/favicon.png" rel="icon">
  <link href="{base_url}elements/assets/img/apple-touch-icon.png" rel="apple-touch-icon">

  <!-- Bootstrap core CSS -->
  <link href="{base_url}elements/assets/lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <!--external css-->
  <link href="{base_url}elements/assets/lib/font-awesome/css/font-awesome.css" rel="stylesheet" />
  <!-- Custom styles for this template -->
  <link href="{base_url}elements/assets/css/style.css" rel="stylesheet">
  <link href="{base_url}elements/assets/css/style-responsive.css" rel="stylesheet">

</head>

<body>
  <!-- **********************************************************************************************************************************************************
      MAIN CONTENT
      *********************************************************************************************************************************************************** -->
  <div id="login-page">
    <div class="container">

      <form class="form-login" id="formAuth" action={authUrl} method="post">
        <h2 class="form-login-heading">Potrero Digital <br/> Plataforma Virtual</h2>
        <div class="login-wrap">


          {if {show_warn}}
          <div class="form-group alert alert-danger">
                   <button type="button" class="close" data-dismiss="alert">&times;</button>
          <strong>{msgcode}</strong>
                </div>
          {/if}


          <input type="text" id="usernameInput" name="username" class="form-control" placeholder="Usuario" autofocus>
          <br>
          <input type="password" id="passwordInput" name="password" class="form-control" placeholder="Contraseña">
          <label class="checkbox">
            <span class="pull-right">
            <a data-toggle="modal" href="login.html#myModal"> Te olvidaste la contraseña?</a>
            </span>
            </label>
          <button class="btn btn-theme btn-block" id="login_submit" type="submit"><i class="fa fa-lock"></i>Entrá</button>
        </form>
          <hr>
        </div>
        <!-- Modal -->
        <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="myModal" class="modal fade">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Recuperar contraseña</h4>
              </div>
              <div class="modal-body">
                <form action="{base_url}perfil/recuperar_contrasena" method="post">
                <p>Ingresa tu E-mail para recuperar la contraseña</p>
                <input type="text" name="email" placeholder="Email" autocomplete="off" class="form-control placeholder-no-fix">
              </div>
              <div class="modal-footer">
                <button data-dismiss="modal" class="btn btn-default" type="button">Cancelar</button>
                <button class="btn btn-theme" type="submit">Enviar</button>
              </div>
            </form>
            </div>
          </div>
        </div>
        <!-- modal -->
    </div>
  </div>
  <!-- js placed at the end of the document so the pages load faster -->
  <script src="{base_url}elements/assets/lib/jquery/jquery.min.js"></script>
  <script src="{base_url}elements/assets/lib/bootstrap/js/bootstrap.min.js"></script>
  <script src="{base_url}alumno/assets/js/app.js"></script>

  <!--BACKSTRETCH-->
  <!-- You can use an image of whatever size. This script will stretch to fit in any screen size.-->
  <script type="text/javascript" src="{base_url}elements/assets/lib/jquery.backstretch.min.js"></script>
  <script>
    $.backstretch("{base_url}elements/assets/img/login-bg.jpg", {
      speed: 500
    });

  </script>
</body>

</html>
