<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Actualiza los archivos segun la rama configurada
 *
 * Este controlador permite actualizar autumaticamente los archivos contenidos en la aplicacion
 * ejecutando el comando GIT: git pull y registrando la salida a un archivo de registro.
 * Este archivo funciona en conjunto con el web-hook de gitorious y no puede ser invocado manualmente
 *
 * @autor Borda Juan Ignacio
 *
 * @version 	.1 (2014-12-03)
 *
 *
 */
class Editor_de_codigo extends MX_Controller {

    function __construct() {
        parent::__construct();
        $this->base_url = base_url();
        $this->module_url = base_url() . $this->router->fetch_module() . '/';
        $this->user->authorize();
        $this->load->module('elements');
        $this->load->helper('file');
        $this->load->helper('url');
        $this->load->library('parser');
        $this->load->model('app');
        $this->load->model('user/user');
        $this->load->model('alumno/Model_alumno');
        $this->load->model('docente/Model_docente');
        $this->idu = $this->user->idu;
        $this->alumno = $this->user->get_user($this->idu);
        error_reporting(0);
        //---Output Profiler
        //$this->output->enable_profiler(TRUE);
    }
    // function Index(){
    //     $this->user->authorize();
    //     $this->code_dashboard();
    // }

    function Index() {
      $data["base_url"] = $this->base_url;
      $data["module_url"] = $this->module_url;
      $data["idu"] = $this->idu;
      $data["head"] = $this->elements->head();
      $data["header"] = $this->elements->ui_header();
      $data["navbar"] = $this->elements->ui_navbar();
      $data["codigo"] = $this->file('index', "es");

      echo $this->parser->parse("index", $data, true, true);
     	//$customData['usercan_create']=true;
    }


    function code_dashboard(){
        $this->user->authorize();
        Modules::run('dashboard/dashboard', 'editor_de_codigo/json/dashboard.json');
    }
    function highlight(){
        $this->user->authorize();
        Modules::run('dashboard/dashboard', 'editor_de_codigo/json/highlight.json');
    }

    function code_block($code, $lang='html',$theme='monokai',$rows=30){
        return '<textarea rows="'.$rows.'" class="code_block" theme="'.$theme.'" lang="'.$lang.'">'.$code.'</textarea>';

    }
    function highlight_block($code, $lang='html',$theme='monokai',$rows=16){
        return '<pre><code class="'.$lang.'" lang="'.$lang.'">'.$code.'</code></pre>';

    }

    function demo($filetype){
        $this->load->helper('file');
        $this->load->module('dashboard');
        $filename= APPPATH . "modules/editor_de_codigo/views/$filetype.php";
        $code=read_file($filename);
        $data['content']=$this->code_block($code,$filetype);
        $data['title']="Demo: ".$filetype;
        $template="dashboard/widgets/box_info_solid";
        echo $this->dashboard->widget($template, $data);
    }
    function demo_highlight($filetype){
        $this->load->helper('file');
        $this->load->module('dashboard');
        $filename=APPPATH . "modules/editor_de_codigo/views/$filetype.php";
        $code=read_file($filename);
        $data['content']=$this->highlight_block($code,$filetype);
        $data['title']="Demo: ".$filetype;
        $template="dashboard/widgets/box_info_solid";
        echo $this->dashboard->widget($template, $data);
    }
    function file($file, $lang="es",$theme='monokai'){
        $this->load->helper('file');
        $this->load->module('dashboard');
        $filename=APPPATH .'modules/editor_de_codigo/assets/'. $this->idu .'.php';
        $code=read_file($filename);

        $data['content']=$this->code_block($code,$lang,$theme);
        $data['title']="File: ".$filename;
        $template="dashboard/widgets/box_info_solid";
        return $this->dashboard->widget($template, $data);
    }
    function highlight_file($file,$lang,$theme='monokai'){
        $this->load->helper('file');
        $this->load->module('dashboard');
        $filename=APPPATH . $file;
        $code=read_file($filename);
        $data['content']=$this->highlight_block($code,$lang,$theme);
        $data['title']="File: ".$filename;
        $template="dashboard/widgets/box_info_solid";
        echo $this->dashboard->widget($template, $data);
    }

}
