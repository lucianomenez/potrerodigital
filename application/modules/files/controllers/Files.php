<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');




class Files extends MX_Controller {

    function __construct() {
      parent::__construct();
      $this->base_url = base_url();
      $this->module_url = base_url() . $this->router->fetch_module() . '/';
      $this->user->authorize();
      error_reporting(E_ERROR | E_PARSE);
    
    }

    function upload_file(){

          $data_post = $this->input->post();
          $data_file = $_FILES['file'];
          $data_image= getimagesize ($data_file['tmp_name']);
          $response=array();
          $milisegundos=round(microtime(true)*1000);

          if(! $data_post['idu']){
            $data_post['idu']='';
          }else{
            $data_post['idu']=(int)$data_post['idu'].'/';
          }
          //Comparo si el archivo subido ya existe
          $array_names_cargados=json_decode($data_post['names']);
          $upload_file=true;
          if(is_array($array_names_cargados)){
              if(in_array($data_file['name'],$array_names_cargados)){
                  $upload_file=false;
              }
          }
          if($upload_file){
              $file_extension = pathinfo($data_file['name'], PATHINFO_EXTENSION);

              // Make dir
              if (!file_exists(FCPATH.'application/modules/files/assets/'.$data_post['folder'].'/'.$data_post['idu'].$data_post['item'].'/'.$data_post['data-filename'])){
                  @mkdir(FCPATH.'application/modules/files/assets/'.$data_post['folder'].'/'.$data_post['idu'].$data_post['item'].'/'.$data_post['data-filename'].'/',0777,true);
              }

              // File configuration
              $config['file_name']            = $data_post['data-filename'] .$milisegundos."-" .$data_post['item'].'.'.$file_extension;
              $config['upload_path']          = FCPATH.'application/modules/files/assets/'.$data_post['folder'].'/'.$data_post['idu'].$data_post['item'].'/'.$data_post['data-filename'].'/';
              $config['allowed_types']        = "gif|jpg|png|doc|DOC|docx|DOCX|jpeg|txt|TXT|JPG|PNG|JPEG|pdf|PDF|mp3|MP3|mp4|MP4|";
              $config['max_size']            = 0;
              $config['overwrite']           = false;

              // Upload
              $this->load->library('upload', $config);

              // Set response data
              $response['uploadUrl'] = $this->base_url.'files/assets/'.$data_post['folder'].'/'.$data_post['idu'].$data_post['item'].'/'.$data_post['data-filename'].'/'.$config['file_name'];

              $response['relativeUrl'] = 'files/assets/'.$data_post['folder'].'/'.$data_post['idu'].$data_post['item'].'/'.$data_post['data-filename'].'/'.$config['file_name'];
              $response['extension']=$file_extension;
              $response['file_extension']=$data_file['type'];
              $response['width']=$data_image[0];
              $response['height']=$data_image[1];
              $response['size']=$data_file['size'];
              $response['original_name']=$data_file['name'];
               $response['name']=  $config['file_name'];
              $response['subida_ok']='ok';
              $response['success']=true;
              if ( ! $this->upload->do_upload('file'))
              {
                  $error = array('error' => $this->upload->display_errors());
                  $error['success']=false;
                  echo json_encode($error);
              }
              else
              {
                  $data = array('upload_data' => $this->upload->data());
                  $data['upload_path'] = $config['upload_path'];
                  echo json_encode($response);
              }
          }else{
            $response['subida_ok']='repetido';
            echo json_encode($response);
          }
        }
    }